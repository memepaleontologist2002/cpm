#!/bin/sh

if ! test -f Makefile.am; then
	cd ..
fi

if ! test -f Makefile.am; then
	echo "Can't find project root!" >&2
	exit 1
fi

function tool() {
	if ! which "$1"; then
		echo "Can't find $1!"
		exit 1
	fi
}

tool autoconf
tool automake
tool libtool
tool autopoint
tool autoreconf
tool make

# Generate doc.mk
autoreconf --install --verbose
./configure --disable-debug --enable-std-eapi --enable-doc --enable-doc-man --enable-doc-html --enable-silent-rules
sh ./scripts/updatedocfilelist.sh
rm -r doc

# Final build
autoreconf --verbose
./configure --disable-debug --enable-std-eapi --enable-doc --enable-doc-man --enable-doc-html --enable-silent-rules
make distcheck
