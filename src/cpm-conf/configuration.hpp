#ifndef BIN_CPM_CONF_CONFIGURATION_HPP
#define BIN_CPM_CONF_CONFIGURATION_HPP

#include <vector>
#include <string>

#include "cpm.hpp"

#include "common.hpp"
#include "configuration_option.hpp"

class configuration {
	public:
		enum action {
			PARSE,
			INTERACTIVE,
			PRINT_HELP,
			PRINT_VERSION
		};
		configuration_option<std::string> prompt;
		configuration_option<std::string> prompt_question;
		configuration_option<cpm::context> cpm;
		configuration_option<std::string> self;
		configuration_option<action> act;
		std::vector<std::string> files;
		configuration_option<bool> exit = false;
		configuration_option<cpm::logging_levels> mask;
		void update(const configuration& c);
		void setLog(const char* changes);
};

#endif
