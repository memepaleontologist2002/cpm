#include "configuration_option.hpp"

#include <algorithm>

configuration_option<cpm::logging_levels>::configuration_option() : has() {}
configuration_option<cpm::logging_levels>::configuration_option(const _T& o) : has(), val(o) {
	for(std::size_t i = 0; i <= (std::size_t) cpm::logging_level::MAX; ++i) {
		has |= (cpm::logging_level) i;
	}
}
configuration_option<cpm::logging_levels>::configuration_option(_T&& o) : has(), val(std::move(o)) {
	for(std::size_t i = 0; i <= (std::size_t) cpm::logging_level::MAX; ++i) {
		has |= (cpm::logging_level) i;
	}
}
configuration_option<cpm::logging_levels>& configuration_option<cpm::logging_levels>::update(const configuration_option<_T> &o) {
	for(std::size_t i = 0; i <= (std::size_t) cpm::logging_level::MAX; ++i) {
		if(o.has.isSet((cpm::logging_level) i)) {
			this->has |= (cpm::logging_level) i;
			if(o.val.isSet((cpm::logging_level) i)){
				this->val |= (cpm::logging_level) i;
			} else {
				this->val.rm((cpm::logging_level) i);
			}
		}
	}
	return *this;
}
configuration_option<cpm::logging_levels>& configuration_option<cpm::logging_levels>::unset() {
	for(std::size_t i = 0; i <= (std::size_t) cpm::logging_level::MAX; ++i) {
		has.rm((cpm::logging_level) i);
	}
	return *this;
}
configuration_option<cpm::logging_levels>& configuration_option<cpm::logging_levels>::operator=(_T&& v) {
	val = std::move(v);
	for(std::size_t i = 0; i <= (std::size_t) cpm::logging_level::MAX; ++i) {
		has |= (cpm::logging_level) i;
	}
	return *this;
}
configuration_option<cpm::logging_levels>& configuration_option<cpm::logging_levels>::operator=(_T& v) {
	val = v;
	for(std::size_t i = 0; i <= (std::size_t) cpm::logging_level::MAX; ++i) {
		has |= (cpm::logging_level) i;
	}
	return *this;
}
configuration_option<cpm::logging_levels>::operator const cpm::logging_levels&() {
	return val;
}
