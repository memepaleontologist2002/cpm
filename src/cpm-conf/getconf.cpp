#include <cstring>
#include <cstdlib>

#include <unistd.h>

#include <vector>
#include <string>
#include <sstream>

#include "cpm.hpp"
#include "config.hpp"

#include "getconf.hpp"
#include "errors.hpp"
#include "logging.hpp"
#include "util.hpp"

configuration parse_options(int argc, char** argv) {
	configuration a;
	int opt_;
	while((opt_ = getopt(argc, argv, "-hil:f:dV")) != -1) {
		char opt = opt_;
		switch(opt) {
			case 'h':
				a.act = configuration::PRINT_HELP;
				break;
			case 'V':
				a.act = configuration::PRINT_VERSION;
				break;
			case 'f':
				a.files.emplace_back(optarg);
				break;
			case 'i':
				a.act = configuration::INTERACTIVE;
				break;
			case 'l':
				a.setLog(optarg);
				break;
			case 'd':
				a.mask.val |= cpm::logging_level::DEBUG;
				a.mask.has |= cpm::logging_level::DEBUG;
				break;
			case '?':
				throw error_invalid_option();
		}
	}
	return a;
}

configuration get_env_conf(configuration& c) {
	configuration r = configuration();
	const char* env;
	if((env = std::getenv("CPMCONF_ACTION")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Setting action to %s\n", env));
		if(std::strcmp(env, "PARSE") == 0 || std::strcmp(env, "parse") == 0) {
			r.act = configuration::action::PARSE;
		} else if(std::strcmp(env, "INTERACTIVE") == 0 || std::strcmp(env, "interactive") == 0) {
			r.act = configuration::action::INTERACTIVE;
		} else if(std::strcmp(env, "HELP") == 0 || std::strcmp(env, "help") == 0) {
			r.act = configuration::action::PRINT_HELP;
		} else if(std::strcmp(env, "VERSION") == 0 || std::strcmp(env, "version") == 0) {
			r.act = configuration::action::PRINT_VERSION;
		} else {
			message_str(c, cpm::logging_level::WARN, format("Unknown action \"%s\" for $CPMCONF_ACTION\n", env));
		}
	}
	if((env = std::getenv("CPMCONF_SELF")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("I am %s\n", env));
		r.self = env;
	}
	if((env = std::getenv("CPMCONF_FILES")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Note for parsing: %s\n", env));
		std::stringstream in(env);
		std::string filepath;
		while(in.good()) {
			in >> filepath;
			r.files.push_back(filepath);
		}
	}
	if((env = std::getenv("CPMCONF_LOG_MASK")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Changing logging mask: %s\n", env));
		r.setLog(env);
	}
	if((env = std::getenv("CPMCONF_PROMPT")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Setting prompt to %s\n", env));
		r.prompt = env;
	}
	if((env = std::getenv("CPMCONF_PROMPT_QUESTION")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Setting question prompt to %s\n", env));
		r.prompt_question = env;
	}
	return r;
}

configuration get_default_conf() {
	static const char* self = CPMCONF_DEFAULT_SELF;
	configuration r = {
		.prompt = CPMCONF_DEFAULT_PROMPT,
		.prompt_question = CPMCONF_DEFAULT_PROMPT_QUESTION,
		.cpm = cpm::context(),
		.self = strdup(self),
		.act = configuration::action::CPMCONF_DEFAULT_ACTION,
		.files = CPMCONF_DEFAULT_FILES,
		.exit = false,
	};
	r.setLog(CPMCONF_DEFAULT_LOG_MASK);
	return r;
}
