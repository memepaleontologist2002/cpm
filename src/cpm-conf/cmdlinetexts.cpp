#include <iostream>

#include "config.h"

#include "cmdlinetexts.hpp"
#include "configuration.hpp"

void help(configuration& c) {
	std::cout << "Usage: " << c.self << " <options>" << std::endl
	          << "Options:" << std::endl
	          << "  -d            Also print logging level DEBUG" << std::endl
	          << "  -f <file>     Check file" << std::endl
	          << "  -h            Print this help message" << std::endl
	          << "  -i            Interactively debug configuration" << std::endl
	          << "  -l <changes>  Change the logging mask" << std::endl
	          << "  -V            Print version information" << std::endl;
}

void version(configuration& c) {
	std::cout << PACKAGE_STRING << std::endl;
}

void interactive_help(configuration& c) {
	std::cout << "List of commands:" << std::endl
	          << "<short> <long> <description>" << std::endl
	          << "  h      help  Print this help" << std::endl
	          << "         log   Change logging mask" << std::endl
	          << "  l      list  list various things" << std::endl
	          << "  p      parse Parse some files" << std::endl
	          << "  x      exit  Exit this program" << std::endl;
}
