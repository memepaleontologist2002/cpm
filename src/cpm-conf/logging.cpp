#include <iostream>

#include "cpm.hpp"

#include "logging.hpp"

void message(cpm::logging_levels mask, cpm::logging_message&& msg) {
	if(msg.message.back() != '\n' && mask & cpm::logging_level::WARN) {
		std::cout << "[WARN]   Following not ending in newline:\n";
	}
	if(!mask.isSet(msg.level)) {
		return;
	}
	switch (msg.level) {
		case cpm::logging_level::DEBUG:
			std::cout << "[DEBUG]  ";
			break;
		case cpm::logging_level::INFO:
			std::cout << "[INFO]   ";
			break;
		case cpm::logging_level::NOTICE:
			std::cout << "[NOTICE] ";
			break;
		case cpm::logging_level::WARN:
			std::cout << "[WARN]   ";
			break;
		case cpm::logging_level::ERROR:
			std::cout << "[ERROR]  ";
			break;
		case cpm::logging_level::CONF_WARN:
			std::cout << "[CONF: WARN]     ";
			break;
		case cpm::logging_level::CONF_SYNTAX_ERROR:
			std::cout << "[CONF: SYNTAX]   ";
			break;
		case cpm::logging_level::CONF_RARE:
			std::cout << "[CONF: RARE]     ";
			break;
		case cpm::logging_level::CONF_UNLIKELY:
			std::cout << "[CONF: UNLIKELY] ";
			break;
		case cpm::logging_level::CONF_COMPLICATED:
			std::cout << "[CONF: COMPLEX]  ";
			break;
		default:
			std::cout << "[Invalid] ";
	}
	std::cout << msg.message;
	if(msg.message.back() != '\n') {
		std::cout << std::endl;
	}
}

void message_str(configuration& c, cpm::logging_level lvl, std::string&& msg) {
	const std::vector<std::string> backtrace_cpm_conf = {"cpm-conf"};
	cpm::logging_message m = {
		.message = std::move(msg),
		.level = lvl,
		.time = std::time(NULL),
		.backtrace = &backtrace_cpm_conf
	};
	message(c.mask, std::move(m));
}

