#ifndef CPM_UTIL_HPP
#define CPM_UTIL_HPP

#include <cstddef>
#include <cstring>

#include <gmpxx.h>

#include "cpm.hpp"

/*!
 * @brief A function for formatting c style strings.
 * 
 * Formats strings like printf.
 * 
 * @param format The format string.
 * @param args The things to format.
 * 
 * @return The formatted c style string.
 */
template<class... Args>
char* format(const char* format, Args... args) {
	size_t len = snprintf(NULL, 0, format, args...);
	if(len < 0) {
		throw cpm::error_format();
	}
	char* r = NULL;
	r = new char[len + 1];
	len = snprintf(r, len + 1, format, std::forward<Args>(args)...);
	r[len] = '\0';
	if(len < 0) {
		throw cpm::error_format();
	}
	return r;
}

#endif
