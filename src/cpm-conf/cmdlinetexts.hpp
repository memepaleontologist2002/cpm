#ifndef BIN_CPM_CONF_CMDLINETEXTS_HPP
#define BIN_CPM_CONF_CMDLINETEXTS_HPP

#include "configuration.hpp"

void help(configuration& c);
void version(configuration& c);
void interactive_help(configuration& c);

#endif
