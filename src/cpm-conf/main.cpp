#include <cstdlib>
#include <unistd.h>

#include <vector>
#include <string>
#include <sstream>
#include <iostream>

#include "cpm.hpp"
#include <config.h>

#include "configuration.hpp"
#include "logging.hpp"
#include "getconf.hpp"
#include "interactive.hpp"
#include "cmdlinetexts.hpp"

int main(int argc, char** argv) {
	try {
		configuration c = get_default_conf();
		c.self = argv[0];
		c.cpm.val.logging_function = [&c](cpm::logging_message&& msg){message(c.mask, std::move(msg));};
		message_str(c, cpm::logging_level::DEBUG, "Starting cpm-conf ...\n");
		
		c.update(get_env_conf(c));
		c.update(parse_options(argc, argv));
		switch(c.act) {
			case configuration::PRINT_HELP:
				help(c);
				break;
			case configuration::PRINT_VERSION:
				version(c);
				break;
			case configuration::INTERACTIVE:
			case configuration::PARSE:
				if(c.files.size() > 0) {
					for(const std::string& s : c.files) {
						std::cout << "Parsing " << s << std::endl;
						c.cpm.val.parse(s);
					}
				} else if(c.act == configuration::PARSE) {
					std::cerr << "No configuration files to parse!" << std::endl;
					return EXIT_FAILURE;
				}
				if(c.act == configuration::PARSE) {
					break;
				}
				interactive(c);
				break;
		}
		
		return EXIT_SUCCESS;
	} catch(const std::exception& e) {
		std::cerr << e.what();
	}
	return EXIT_FAILURE;
}
