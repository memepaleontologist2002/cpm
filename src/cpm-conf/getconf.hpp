#ifndef BIN_CPM_CONF_GET_CONFIGURATION_HPP
#define BIN_CPM_CONF_GET_CONFIGURATION_HPP

#include "configuration.hpp"

configuration get_env_conf(configuration& c);
configuration parse_options(int argc, char** argv);
configuration get_default_conf();

#endif
