#include <cstddef>

#include <vector>
#include <string>
#include <iostream>
#include <sstream>

#include "interactive.hpp"
#include "cmdlinetexts.hpp"
#include "configuration.hpp"
#include "logging.hpp"
#include "util.hpp"

void interactive(configuration& c) {
	std::string command;
	std::string current_word;
	std::vector<std::string> parsed;
	while(!c.exit) {
		parsed.clear();
		std::cout << c.prompt;
		std::getline(std::cin, command);
		std::stringstream inParser(command);
		while(inParser.good()) {
			inParser >> current_word;
			parsed.push_back(std::move(current_word));
		}
		if(parsed.size() == 0) {
			continue;
		} else if(parsed[0] == "exit" || parsed[0] == "x") {
			c.exit = 1;
		} else if(parsed[0] == "help" || parsed[0] == "h") {
			interactive_help(c);
		} else if(parsed[0] == "list" || parsed[0] == "l") {
			interactive_list(c, parsed);
		} else if(parsed[0] == "log") {
			interactive_log(c, parsed);
		} else if(parsed[0] == "parse" || parsed[0] == "p") {
			interactive_parse(c, parsed);
		} else {
			std::cout << "Command not found!" << std::endl
			          << "Type help for help" << std::endl;
		}
	}
}

const std::string& interactive_select(configuration& c, const std::vector<std::string>& selections, const std::string& what, const std::string& def) {
	/* |Select repository:
	 * |
	 * |  1. repo1
	 * |  2. repo2
	 * |
	 * |> 1
	 */
	if(what.size() > 0) {
		// Intentionally double newline
		std::cout << format("Select %s:\n", what.c_str()) << std::endl;
	}
	{ // Scope for size_size (and formatstring)
		std::size_t size_size = 0;
		for(std::size_t len = selections.size(); len > 0; ++size_size) {
			len /= 10;
		}
		char* formatstring = format("%%%zuzu. %%s\n", size_size);
		for(std::size_t i = 0; i < selections.size(); ++i) {
			std::cout << format(formatstring, i + 1, selections[i].c_str());
		}
	}
	std::cout << std::endl << c.prompt_question;
	
	std::string input;
	getline(std::cin, input);
	if(isspace(input.back())) {
		message_str(c, cpm::logging_level::DEBUG, "Removing trailing whitespace\n");
	}
	while(input.size() > 0 && isspace(input.back())) {
		input.pop_back();
#ifndef NDEBUG
		message_str(c, cpm::logging_level::DEBUG, format("Now: >>%s<<\n", input.c_str()));
#endif
	}
	const char* c_input_rmspace = input.c_str();
	if(isspace(*c_input_rmspace)) {
		message_str(c, cpm::logging_level::DEBUG, "Removing leading whitespace\n");
	}
	while (*c_input_rmspace != '\0' && isspace(*c_input_rmspace)) {
		++c_input_rmspace;
#ifndef NDEBUG
		message_str(c, cpm::logging_level::DEBUG, format("Now: >>%s<<\n", c_input_rmspace));
#endif
	}
	message_str(c, cpm::logging_level::DEBUG, format("After trimming: >>%s<<\n", c_input_rmspace));
	char* at;
	unsigned long long inNum = std::strtoull(c_input_rmspace, &at, 10);
	if(*at == '\0' && inNum > 0 && inNum <= selections.size()) {
		return selections[inNum - 1];
	} else {
		auto begin = selections.begin();
		auto end = selections.end();
		auto r = begin;
		if((r = std::find(begin, end, c_input_rmspace)) != selections.end()) {
			return *r;
		} else {
			return def;
		}
	}
}

void interactive_log(configuration &c, std::vector<std::string> &parsed) {
	auto iter = parsed.begin();
	++iter;
	auto iter_end = parsed.end();
	for(; iter != iter_end; ++iter) {
		const std::string& i = *iter;
		c.setLog(i.c_str());
	}
}

void interactive_display_list(configuration& c, const std::vector<std::string>& items, const std::string& what) {
	if(what != "") {
		std::cout << format("List of %s:\n", what.c_str());
	} else {
		std::cout << std::endl;
	}
	for(const std::string& i : items) {
		std::cout << format("  - %s\n", i.c_str());
	}
	std::cout << std::endl;
}

void interactive_list(configuration& c, std::vector<std::string>& parsed) {
	if(parsed.size() == 1) {
		std::string a = interactive_select(c, {"repositories"}, "something to list", "");
		if(a == "") {
			return;
		}
		parsed.push_back(a);
	}
	for(const std::string& i : parsed) {
		if(i == "repositories") {
			std::vector<std::string> displaylist;
			for(const cpm::repository* repo : c.cpm.val.conf.repositories) {
				displaylist.push_back(std::move((std::string)*repo->getNameUserPreferred()));
			}
			interactive_display_list(c, displaylist, "repositories");
		}
	}
}

void interactive_parse(configuration& c, std::vector<std::string>& parsed) {
	if(parsed.size() == 1) {
		std::cout << "parse ";
		std::string all;
		std::getline(std::cin, all);
		std::stringstream inParser(std::move(all));
		std::string current_word;
		while(inParser.good()) {
			inParser >> current_word;
			parsed.push_back(std::move(current_word));
		}
	}
	if(parsed.size() == 1) {
		std::cout << "No input file to parse was given." << std::endl;
	}
	auto iter = parsed.begin();
	++iter;
	auto iter_end = parsed.end();
	for(; iter != iter_end; ++iter) {
		const std::string& i = *iter;
		c.cpm.val.parse(i);
	}
}
