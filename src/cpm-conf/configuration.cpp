#include "configuration.hpp"

#include "logging.hpp"
#include "util.hpp"

#include "cpm.hpp"

void configuration::update(const configuration &c) {
	prompt.update(c.prompt);
	prompt_question.update(c.prompt_question);
	cpm.update(c.cpm);
	self.update(c.self);
	act.update(c.act);
	files += c.files;
	exit.update(c.exit);
	mask.update(c.mask);
}

void configuration::setLog(const char *changes) {
	cpm::logging_levels tmpmask = mask;
	bool add = true;
	bool conf = false;
	message_str(*this, cpm::logging_level::DEBUG, format("Interpreting logging level mask change %s\n", changes));
	for(const char* at = changes; *at != '\0'; ++at) {
#define level(lvl) if(add) {if(!conf) {tmpmask |= cpm::logging_level::lvl;}} else {if(!conf) {tmpmask.rm(cpm::logging_level::lvl);}}
#define level_c(lvl, clvl) if(add) {if(conf) {tmpmask |= cpm::logging_level::clvl;} else {tmpmask |= cpm::logging_level::lvl;}} else {if(conf) {tmpmask.rm(cpm::logging_level::clvl);} else {tmpmask.rm(cpm::logging_level::lvl);}}
#define clevel(lvl) if(add) {if(conf) {tmpmask |= cpm::logging_level::lvl;}} else {if(conf) {tmpmask.rm(cpm::logging_level::lvl);}}
		message_str(*this, cpm::logging_level::DEBUG, format("conf = %s\n", conf? "true" : "false"));
		switch(std::tolower(*at)) {
			case 'c':
				conf = true;
				continue;
			case '+':
				add = true;
				break;
			case '-':
				add = false;
				break;
			case 'd':
				if(add) {
					message_str(*this, cpm::logging_level::DEBUG, "Adding logging level DEBUG\n");
				} else {
					message_str(*this, cpm::logging_level::DEBUG, "Removing logging level DEBUG\n");
				}
				level(DEBUG);
				break;
			case 'i':
				if(add) {
					message_str(*this, cpm::logging_level::DEBUG, "Adding logging level INFO\n");
				} else {
					message_str(*this, cpm::logging_level::DEBUG, "Removing logging level INFO\n");
				}
				level(INFO);
				break;
			case 'n':
				if(add) {
					message_str(*this, cpm::logging_level::DEBUG, "Adding logging level NOTICE\n");
				} else {
					message_str(*this, cpm::logging_level::DEBUG, "Removing logging level NOTICE\n");
				}
				level(NOTICE)
				break;
			case 'w':
				if(add) {
					message_str(*this, cpm::logging_level::DEBUG, "Adding logging level WARN\n");
				} else {
					message_str(*this, cpm::logging_level::DEBUG, "Removing logging level WARN\n");
				}
				level_c(WARN, CONF_WARN);
				break;
			case 'e':
				if(add) {
					message_str(*this, cpm::logging_level::DEBUG, "Adding logging level ERROR\n");
				} else {
					message_str(*this, cpm::logging_level::DEBUG, "Removing logging level ERROR\n");
				}
				level_c(ERROR, CONF_SYNTAX_ERROR);
				break;
			case 'x':
				if(add) {
					message_str(*this, cpm::logging_level::DEBUG, "Adding logging level CONF_COMPLICATED\n");
				} else {
					message_str(*this, cpm::logging_level::DEBUG, "Removing logging level CONF_COMPLICATED\n");
				}
				clevel(CONF_COMPLICATED);
				break;
			case 'r':
				if(add) {
					message_str(*this, cpm::logging_level::DEBUG, "Adding logging level CONF_RARE\n");
				} else {
					message_str(*this, cpm::logging_level::DEBUG, "Removing logging level CONF_RARE\n");
				}
				clevel(CONF_RARE);
				break;
			case 'u':
				if(add) {
					message_str(*this, cpm::logging_level::DEBUG, "Adding logging level CONF_UNLIKELY\n");
				} else {
					message_str(*this, cpm::logging_level::DEBUG, "Removing logging level CONF_UNLIKELY\n");
				}
				clevel(CONF_UNLIKELY);
				break;
			case ' ':
				message_str(*this, cpm::logging_level::WARN, format("Skipping %c\n", *at));
				continue;
		}
#undef level
#undef level_c
#undef clevel
		conf = false;
	}

	message_str(*this, cpm::logging_level::DEBUG, "Applying new mask\n");
	mask = tmpmask;
}
