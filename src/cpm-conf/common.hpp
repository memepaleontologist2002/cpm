#ifndef BIN_CPM_CONF_COMMON_HPP
#define BIN_CPM_CONF_COMMON_HPP

#include <vector>

template<class _T>
std::vector<_T>& operator+=(std::vector<_T>& l, const std::vector<_T>& r) {
	l.reserve(r.size());
	for(const _T& i : r) {
		l.push_back(i);
	}
	return l;
}

#endif
