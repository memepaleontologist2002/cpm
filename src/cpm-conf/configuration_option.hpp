#ifndef BIN_CPM_CONF_CONFIGURATION_OPTION_HPP
#define BIN_CPM_CONF_CONFIGURATION_OPTION_HPP

#include <utility>
#include <iostream>
#include <algorithm>

#include "cpm.hpp"

template<class _T>
class configuration_option {
	public:
		bool has = false;
		_T val;
		configuration_option() : has(false) {}
		configuration_option(const configuration_option<_T>& o) : has(o.has), val(o.val) {}
		configuration_option(configuration_option<_T>&& o) : has(std::move(o.has)), val(std::move(o.val)) {}
		template<class... Args>
		configuration_option(Args... args) : has(true), val(std::forward<Args>(args)...) {}
		
		configuration_option<_T>& update(const configuration_option<_T>& o) {
			if(o.has) {
				has = true;
				val = o.val;
			}
			return *this;
		}
		configuration_option<_T>& unset() {
			has = false;
			return *this;
		}
		template<class Arg>
		configuration_option<_T>& operator=(Arg&& v) {
			has = true;
			val = std::move(v);
			return *this;
		}
		template<class Arg>
		configuration_option<_T>& operator=(Arg& v) {
			has = true;
			val = v;
			return *this;
		}
		operator const _T&() {
			return val;
		}
};

template<>
class configuration_option<cpm::logging_levels>;

template<>
class configuration_option<cpm::logging_levels> {
	public:
		typedef cpm::logging_levels _T;
		_T has;
		_T val;
		configuration_option();
		configuration_option(const _T& o);
		configuration_option(_T&& o);
		
		configuration_option<_T>& update(const configuration_option<_T>& o);
		configuration_option<_T>& unset();
		configuration_option<_T>& operator=(_T&& v);
		configuration_option<_T>& operator=(_T& v);
		operator const _T&();
};

template<class _T>
std::ostream& operator<<(std::ostream& s, const configuration_option<_T> o) {
	if(o.has) {
		s << o.val;
	}
	return s;
}

#endif
