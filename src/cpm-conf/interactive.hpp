#ifndef BIN_CPM_CONF_INTERACTIVE_HPP
#define BIN_CPM_CONF_INTERACTIVE_HPP

#include <vector>
#include <string>

#include "configuration.hpp"

void interactive(configuration& c);
const std::string& interactive_select(configuration& c, const std::vector<std::string>& selections, const std::string& what = "", const std::string& def = "");
void interactive_log(configuration& c, std::vector<std::string>& parsed);
void interactive_display_list(configuration& c, const std::vector<std::string>& items, const std::string& what = "");
void interactive_list(configuration& c, std::vector<std::string>& parsed);
void interactive_parse(configuration& c, std::vector<std::string>& parsed);

#endif
