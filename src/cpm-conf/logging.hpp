#ifndef BIN_CPM_CONF_LOGGING_HPP
#define BIN_CPM_CONF_LOGGING_HPP

#include "cpm.hpp"

#include "configuration.hpp"

void message_str(configuration& c, cpm::logging_level lvl, std::string&& msg);
void message(cpm::logging_levels mask, cpm::logging_message&& msg);

#endif
