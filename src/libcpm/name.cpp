#include "cpm.hpp"

namespace cpm {
	// name
	name::~name() {}
	
	bool name::operator!=(const name& o) const {
		return !(*this == o);
	}
	
	name::operator std::string() const {
		return toString();
	}
	name::operator char*() const {
		return toCharArray();
	}
}
