#ifndef CPM_UTIL_HPP
#define CPM_UTIL_HPP

#include <cstddef>
#include <cstring>

#include <gmpxx.h>

#include "cpm.hpp"

namespace cpm {
	/*!
	 * @brief A function for formatting c style strings.
	 * 
	 * Currently not in public API.
	 * 
	 * Formats strings like printf.
	 * 
	 * @param format The format string.
	 * @param args The things to format.
	 * 
	 * @return The formatted c style string.
	 */
	template<class... Args>
	char* format(const char* format, Args... args) {
		size_t len = snprintf(NULL, 0, format, args...);
		if(len < 0) {
			throw cpm::error_format();
		}
		char* r = NULL;
		r = new char[len + 1];
		len = snprintf(r, len + 1, format, std::forward<Args>(args)...);
		r[len] = '\0';
		if(len < 0) {
			throw cpm::error_format();
		}
		return r;
	}
	
	/*!
	 * @brief A function for comparing the next characters.
	 * 
	 * Currently not in the public API.
	 * 
	 * This returns true if the first c style string given starts
	 * with the second.
	 * 
	 * @param first The string which should start with the second.
	 * @param second The prefix the first string should start with.
	 * 
	 * @return True if first starts with second, false otherwise.
	 */
	bool strprefix(const char *first, const char *second);
	
	/*!
	 * @brief Compare strings and go to end.
	 * 
	 * Similar to @ref strprefix(const char *first, const char *second)
	 * but moves the first char* to the end of the prefix if it starts
	 * with it.
	 * 
	 * @param first The string to compare (and move).
	 * @param second The prefix.
	 */
	bool strprefix_mv(const char*& first, const char *second);
	
	/*!
	 * @brief Compares C strings by ASCII.
	 * 
	 * Currently not in the public API.
	 * 
	 * Goes through the whole string and at the first character which
	 * is different from the other string, returns values similar to
	 * c++20 <=>.
	 * 
	 * @param first The first string to compare.
	 * @param second The second string to compare.
	 * @return A value similar to c++20 <=>, determining whichs ASCII
	 *         representation is higher.
	 */
	std::int_fast8_t strasciicmp(const char* first, const char* second);
	
	/*!
	 * @brief strtoull for mpz_class.
	 * 
	 * Currently not in the public API.
	 * 
	 * Does override the given pointer instead of doing it like strtoull.
	 * 
	 * @param str The string to convert (gets moved).
	 * @return The converted value, or 0 if no valid character was there.
	 */
	mpz_class strtompz(const char*& str);
	
	/*!
	 * @brief An unlimited unsigned 0-prefixed integer.
	 * 
	 * Currently not in the public API.
	 * 
	 * This means a full number which is positive and can be prefixed
	 * with zeros, meaning that `001` is different than `1`.
	 */
	class nullprefixeduint {
		public:
			/*!
			 * @brief The count of zeros prefixing this number.
			 */
			mpz_class zeros = 0;
			
			/*!
			 * @brief The actual value.
			 */
			mpz_class value = 0;
			
			/*!
			 * @brief Converts a string to a zero-prefixed unsigned integer.
			 */
			nullprefixeduint(const char*& o);
			
			/*!
			 * @brief Converts this to a string.
			 */
			operator std::string() const;
			
			/*!
			 * @brief Returns whether this > o.
			 * 
			 * @param o The other number to compare.
			 * @return Whether this is greater.
			 */
			bool operator>(const nullprefixeduint& o) const;
			
			/*!
			 * @brief Returns whether this < o.
			 * 
			 * @param o The other number to compare.
			 * @return Whether this is smaller.
			 */
			bool operator<(const nullprefixeduint& o) const;
			
			/*!
			 * @brief Whether two unsigned zero-prefixed numbers are the same.
			 * 
			 * Here, 01 == 001. The count of zeros is fully ignored.
			 */
			bool operator==(const nullprefixeduint& o) const;
			
			/*!
			 * @brief Whether two unsigned zero-prefixed numbers aren't the same.
			 * 
			 * Here, 01 == 001. The count of zeros is fully ignored.
			 */
			bool operator!=(const nullprefixeduint& o) const;
			
			/*!
			 * @brief Returns whether this > o or this == o.
			 * 
			 * @param o The other number to compare.
			 * @return Whether this is greater or equal.
			 */
			bool operator>=(const nullprefixeduint& o) const;
			
			/*!
			 * @brief Returns whether this < o or this == o.
			 * 
			 * @param o The other number to compare.
			 * @return Whether this is smaller or equal.
			 */
			bool operator<=(const nullprefixeduint& o) const;
	};
}

std::ostream& operator<<(std::ostream& s, const cpm::nullprefixeduint& n);

#endif
