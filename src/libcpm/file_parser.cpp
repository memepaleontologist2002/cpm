#include "cpm.hpp"

#include <cstdint>

namespace cpm {
	bool file_parser_correctness::syntax_error() {
		if(syntax_error_count < UINT8_MAX) {
			++syntax_error_count;
		}
		if(syntax_error_count == UINT8_MAX) {
			return true;
		}
		return false;
	}
	void file_parser_correctness::variable_use() {
		if(used_variables < UINT8_MAX) {
			++used_variables;
		}
	}
}
