#include "cpm.hpp"

namespace cpm {
	// version
	version::~version() {}
	
	bool version::operator!=(const version &o) const {
		return !(*this == o);
	}
	bool version::operator<=(const version &o) const {
		return (*this < o) || (*this == o);
	}
	bool version::operator>=(const version &o) const {
		return (*this > o) || (*this == o);
	}
}
