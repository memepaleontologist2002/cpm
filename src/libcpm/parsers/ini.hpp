#ifndef CPM_PARSERS_RAW_INI_HPP
#define CPM_PARSERS_RAW_INI_HPP

#include "cpm.hpp"

#include <istream>
#include <string>
#include <unordered_map>

namespace cpm {
	namespace parsers {
		namespace ini {
			typedef std::unordered_map<std::string, std::string> section;
			typedef std::unordered_map<std::string, section> ini;
			
			/*!
			 * @brief Parse an ini file.
			 * 
			 * It is the callers responsibility to open the file.
			 * 
			 * @param stream The stream to read the file from.
			 * @return The parsed ini structure.
			 */
			ini parse(std::basic_istream<char>& f, file_parser_correctness& c, const file_parser_configuration& conf);
		}
	}
}

#endif
