#include "all.hpp"

#include "cpm.hpp"

#include <vector>

#ifdef ENABLE_STD_EAPI
# include "portage/repos.hpp"
#endif

namespace cpm {
	namespace parsers {
		namespace all {
			std::vector<cpm::file_parser<cpm::configuration>> configuration_file_parsers() {
				std::vector<cpm::file_parser<cpm::configuration>> r;
#ifdef ENABLE_STD_EAPI
				r.emplace_back(&cpm::parsers::portage::repo);
#endif
				return r;
			}
		}
	}
}
