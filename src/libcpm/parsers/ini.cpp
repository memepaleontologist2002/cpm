#include "ini.hpp"

#include <cstddef>
#include <cstring>

#include <algorithm>
#include <fstream>

#include "util.hpp"
#include "charset.hpp"

#ifndef NDEBUG
#include <iostream>
#endif

namespace cpm {
	namespace parsers {
		namespace ini {
			ini parse(std::basic_istream<char>& stream, file_parser_correctness& c, const file_parser_configuration& conf) {
				ini r;
				std::string section_name;
				section sect;
				std::array<char, 256> line;
				const static charset variable_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+_.-@0123456789";
				
				while(!(stream.getline(line.data(), line.size()).eof())) {
					// Find first non-whitespace character
					char* first = line.data();
					while(std::isspace(*first) && *first != '\0') {
						++first;
					}
					
					if(*first == '\0') {
						// empty line
						continue;
					}
					
					std::size_t length = std::strlen(first);
					if(first[0] == '[' && first[length - 1] == ']') {
						// [section]
						// For now, sections can contain any name
						r[std::move(section_name)] = std::move(sect);
						line[length - 1] = '\0';
						section_name = line.data() + 1;
						continue;
					}
					
					if(
						*first == '#' ||
						*first == ';' ||
						(first[0] == '/' && first[1] == '/')
					) {
						// comment
						continue;
					}
					
					if(std::strchr(first, '=') != NULL) {
						// var = value
						
						// Variable name begin
						char* var_beg = first;
						
						// Variable name end
						char* var_end = var_beg;
						do {
							++var_end;
						} while(variable_chars.contains(*var_end) && *var_end != '\0');
						
						// Value begin
						char* val_beg = var_end;
						while(*val_beg != '=' && *val_beg != '\0' && std::isspace(*val_beg)) {
							++val_beg;
						}
						if(*val_beg == '\0') {
							// String terminated too early
							c.syntax_error();
							conf.syntax_warn(format("Syntax error: %s\nMissing =\n", line.data()));
							// Skip line
							continue;
						} else if(*val_beg != '=') {
							// Invalid syntax like
							// varname sth = value
							// varname:= value
							c.syntax_error();
							conf.syntax_warn(format("Syntax error: %s\nInvalid symbols before =\n", line.data()));
							continue;
						}
						// Just had =, skip whitespace
						do {
							++val_beg;
						} while(std::isspace(*val_beg) && *val_beg != '\0');
						
						// Value end
						char* val_end = val_beg + std::strlen(val_beg);
						do {
							--val_end;
						} while(std::isspace(*val_end));
						++val_end;
						
						// Mark ends
						*var_end = '\0';
						*val_end = '\0';
						
						// Saving
						sect[var_beg] = val_beg;
					} else {
						// Invalid syntax such as
						// abc def
						c.syntax_error();
						conf.syntax_warn(format("Syntax error: %s\nInvalid line\n", line.data()));
						// Skip line
						continue;
					}
				}
				r[std::move(section_name)] = std::move(sect);
				return r;
			}
		}
	}
}
