#ifndef CPM_PARSERS_ALL_HPP
#define CPM_PARSERS_ALL_HPP

#include "cpm.hpp"

#include <vector>

namespace cpm {
	namespace parsers {
		namespace all {
			/*!
			 * @brief Create a list of all configuration file parsers.
			 */
			std::vector<cpm::file_parser<cpm::configuration>> configuration_file_parsers();
		}
	}
}

#endif
