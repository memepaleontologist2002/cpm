#ifndef CPM_PARSER_PORTAGE_REPOS_HPP
#define CPM_PARSER_PORTAGE_REPOS_HPP

#include "cpm.hpp"

namespace cpm {
	namespace parsers {
		namespace portage {
			file_parser_correctness repo(std::basic_istream<char>& stream, configuration& c, const file_parser_configuration& conf);
		}
	}
}

#endif
