#include "cpm.hpp"
#include "repos.hpp"

#include "util.hpp"

#include "../ini.hpp"
#include "standards/eapi7/names.hpp"
#include "standards/eapi7/pkgnodes.hpp"

namespace cpm {
	namespace parsers {
		namespace portage {
			file_parser_correctness repo(std::basic_istream<char>& stream, configuration& c, const file_parser_configuration& conf) {
				file_parser_correctness r;
				conf.debug("Parsing as portage repository\n");
				cpm::parsers::ini::ini parsed_ini = cpm::parsers::ini::parse(stream, r, conf);
				for(const std::pair<std::string, cpm::parsers::ini::section>& s : parsed_ini) {
					conf.debug(format("Parsing repository %s\n", s.first.c_str()));
					r.variable_use();
					auto pos = s.second.find("location");
					if(pos != s.second.end()) {
						r.variable_use();
						cpm::standards::eapi7::repo_name name = s.first;
						cpm::standards::eapi7::repository_form form = {
							.location = pos->second,
							.nameUserPreferred = &name
						};
						cpm::standards::eapi7::repository* repo = new cpm::standards::eapi7::repository(form);
						c.repositories.push_back(repo);
						conf.debug(format("Repository %s has location %s\n", s.first.c_str(), pos->second.c_str()));
					}
				}
				conf.debug(format("Now there are %zu repositories\n", c.repositories.size()));
				return r;
			}
		}
	}
}
