#include "cpm.hpp"

namespace cpm {
	pkgnode::~pkgnode() {}
	bool pkgnode::operator!=(const pkgnode &other) const {
		return !(*this == other);
	}
}
