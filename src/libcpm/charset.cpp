#include "charset.hpp"

#include <string>
#include <bitset>

namespace cpm {
	charset::charset(const std::string& o) {
		for(char i : o) {
			content[(unsigned char) i] = true;
		}
		content[0] = true;
	}
	
	charset::charset(const char* o) {
		for(const char* i = o; *i != '\0'; ++i) {
			content[(unsigned char)*i] = true;
		}
		content[0] = true;
	}
	
	bool charset::contains(const std::string& s) const {
		for(const char i : s) {
			if(!content[(unsigned char) i]) {
				return false;
			}
		}
		return true;
	}
	
	bool charset::contains(const char* s) const {
		for(const char* i = s; *i != '\0'; ++i) {
			if(!content[(unsigned char)*i]) {
				return false;
			}
		}
		return true;
	}
	
	bool charset::contains(char c) const {
		return content[(unsigned char) c];
	}
}
