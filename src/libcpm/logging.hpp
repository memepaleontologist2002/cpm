#ifndef CPM_LOGGING_HPP
#define CPM_LOGGING_HPP

#include "cpm.hpp"
#include "util.hpp"

#include <string>
#include <vector>
#include <utility>
#include <functional>

namespace cpm {
	class log;
	
	/*!
	 * @brief A logging system.
	 * 
	 * It works by saving a list of the log messages until it gets the ability to log all of them.
	 * 
	 * It automatically extracts the logging function from the @ref cpm::context, if given.
	 */
	class log {
		private:
			/*!
			 * @brief The context doing the logging.
			 */
			context& ctx;
		public:
			/*!
			 * @brief Creates a log for a sub-process.
			 * 
			 * Every logging function should have one.
			 */
			log(context&, const std::string&);
			
			/*!
			 * @copybrief cpm::log::log(context&, const std::string&)
			 * 
			 * @copydoc cpm::log::log(context&, const std::string&)
			 */
			log(context&, std::string&&);
			
			~log();
			
			/*!
			 * @brief Log a general message.
			 * 
			 * @param level The logging level.
			 * @param message The format string for the message.
			 * @param formatargs The things to insert into the message.
			 */
			template<class... Formatargs>
			void message(logging_level level, const std::string& message, Formatargs... formatargs) {
				logging_message l = {
					.message = format(message.c_str(), std::forward<Formatargs>(formatargs)...),
					.level = level,
					.time = std::time(NULL),
					.backtrace = &ctx.backtrace
				};
				ctx.logging_function(std::move(l));
			}
			
			/*!
			 * @brief Log a general message.
			 * 
			 * @copydoc cpm::log::message
			 */
			template<class... Formatargs>
			void operator()(logging_level level, const std::string& message, Formatargs... formatargs) {
				this->message(level, message, std::forward<Formatargs>(formatargs)...);
			}
			
			/*!
			 * @brief Log a debugging message.
			 * 
			 * @param message The format string for the message.
			 * @param formatargs The things to insert into the message.
			 */
			template<class... Formatargs>
			void debug(const std::string& message, Formatargs... formatargs) {
				this->message(logging_level::DEBUG, message, std::forward<Formatargs>(formatargs)...);
			}
			
			/*!
			 * @brief Log an info message.
			 * 
			 * @param message The format string for the message.
			 * @param formatargs The things to insert into the message.
			 */
			template<class... Formatargs>
			void info(const std::string& message, Formatargs... formatargs) {
				this->message(logging_level::INFO, message, std::forward<Formatargs>(formatargs)...);
			}
			
			/*!
			 * @brief Log a notice message.
			 * 
			 * @param message The format string for the message.
			 * @param formatargs The things to insert into the message.
			 */
			template<class... Formatargs>
			void note(const std::string& message, Formatargs... formatargs) {
				this->message(logging_level::NOTICE, message, std::forward<Formatargs>(formatargs)...);
			}
			
			/*!
			 * @brief Log a warning message.
			 * 
			 * @param message The format string for the message.
			 * @param formatargs The things to insert into the message.
			 */
			template<class... Formatargs>
			void warn(const std::string& message, Formatargs... formatargs) {
				this->message(logging_level::WARN, message, std::forward<Formatargs>(formatargs)...);
			}
			
			/*!
			 * @brief Log an error message.
			 * 
			 * @param message The format string for the message.
			 * @param formatargs The things to insert into the message.
			 */
			template<class... Formatargs>
			void error(const std::string& message, Formatargs... formatargs) {
				this->message(logging_level::ERROR, message, std::forward<Formatargs>(formatargs)...);
			}
	};
}

#endif
