#include "cpm.hpp"

#include <fstream>
#include <functional>

#include "logging.hpp"
#include "parsers/all.hpp"

namespace cpm {
	configuration::configuration() {}
	void configuration::update(context& ctx, configuration &o) {
		log log(ctx, "configuration::update");
		for(repository* i : o.repositories) {
			for(repository* c : this->repositories) {
				if(*i == *c) {
					log.message(logging_level::CONF_WARN, "Redefinition of repository %s\n", i->getNameUserPreferred()->toString().c_str());
					goto loopend;
				}
			}
			// Not in there
			repositories.push_back(i);
		loopend:
			continue;
		}
	}
	
	context::context() : conf(), configuration_file_parsers(cpm::parsers::all::configuration_file_parsers()) {}
	void context::parse(const std::string &path) {
		log log(*this, "context::parse");
		log.debug("Trying to parse configuration file %s\n", path.c_str());
		std::ifstream stream(path);
		if(stream.is_open()){
			log.debug("Successfully opened configuration file\n");
			bool found = false;
			const auto begin = stream.tellg();
			configuration best_conf;
			file_parser_correctness best_c;
#define logcmd(lvl) [&log](const std::string& message){log.message(logging_level::lvl, message);}
			const file_parser_configuration c = {
				.error = logcmd(ERROR),
				.syntax_error = logcmd(CONF_SYNTAX_ERROR),
				.warn = logcmd(WARN),
				.syntax_warn = logcmd(CONF_WARN),
				.syntax_unlikely = logcmd(CONF_UNLIKELY),
				.syntax_complicated = logcmd(CONF_COMPLICATED),
				.syntax_rare = logcmd(CONF_RARE),
				.debug = logcmd(DEBUG)
			};
#undef logcmd
			for(file_parser<configuration>& parser : configuration_file_parsers) {
				stream.seekg(begin);
				configuration current;
				file_parser_correctness corr = parser(stream, current, c);
				if(corr.recoverable) {
					// Found something usable
					if(!found) {
						found = true;
					} else {
#define chkshoulduse(a) if(corr.a < best_c.a) {continue;}
						chkshoulduse(syntax_error_count);
						chkshoulduse(overwrite);
						chkshoulduse(used_variables);
#undef chkshoulduse
					}
					best_conf = current;
					best_c = corr;
				}
			}
			if(found) {
				log.debug("Found correct file parser\n");
			} else {
				log.warn("Could not detect file format\n");
				throw error_file_format();
			}
			conf.update(*this, best_conf);
		} else {
			log.warn("Could not open %s\n", path.c_str());
			throw error_file_open();
		}
	}
}
