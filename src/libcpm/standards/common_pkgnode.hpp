#ifndef CPM_COMMON_PKGNODE_HPP
#define CPM_COMMON_PKGNODE_HPP

#include "cpm.hpp"

namespace cpm {
	/*!
	 * @brief A common repository standard.
	 * 
	 * It implements some commonly used things.
	 * 
	 * Currently, this is only name management.
	 * However, this already is a method and a variable
	 * less to define.
	 * 
	 * @sa cpm::common_pkgnode
	 */
	class common_repository : public repository {
		private:
			/*!
			 * @brief The user preferred name of this pkgnode.
			 */
			name* nameUserPreferred;
		public:
			/*!
			 * @brief Construct a repository with a name.
			 * 
			 * @param nameUserPreferred The name the user prefers.
			 */
			common_repository(const name* nameUserPreferred);
			
			virtual common_repository* clone() const override = 0;
			
			virtual const name* getNameUserPreferred() const final override;
	};
	
	/*!
	 * @brief A common pkgnode standard.
	 * 
	 * It implements some commonly used things.
	 * 
	 * Currently, this is only name management.
	 * However, this already is a method and a variable
	 * less to define.
	 * 
	 * @sa cpm::common_repository
	 */
	class common_pkgnode : public pkgnode {
		private:
			/*!
			 * @brief The user preferred name of this pkgnode.
			 */
			name* nameUserPreferred;
		public:
			/*!
			 * @brief Construct a pkgnode with a name.
			 * 
			 * @param nameUserPreferred The name the user prefers.
			 */
			common_pkgnode(const name* nameUserPreferred);
			
			virtual common_pkgnode* clone() const override = 0;
			
			virtual const name* getNameUserPreferred() const final override;
	};
}

#endif
