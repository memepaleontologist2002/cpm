#include <cstdint>
#include <cstring>

#include <string>
#include "semver2.hpp"

#include <sstream>

#include "cpm.hpp"
#include "util.hpp"

namespace cpm {
	namespace standards {
		const void* semver2::stdIdentifier() const {
			const static char a = 'a';
			return &a;
		}
		
		bool semver2::operator==(const cpm::version& o) const {
			if(this->stdIdentifier() == o.stdIdentifier()) {
				const semver2* s = (semver2*)&o;
				return this->major == s->major && this->minor == s->minor && this->patch == s->patch;
			} else {
				return false;
			}
		}
		
		bool semver2::operator >(const cpm::version& o) const {
			if(this->stdIdentifier() == o.stdIdentifier()) {
				const semver2* f = this;
				const semver2* s = (semver2*)&o;
				return f->major > s->major? true :
					f->major < s->major? false :
					f->minor > s->minor? true :
					f->minor < s->minor? false :
					f->patch > s->patch? true : false;
			} else {
				return false;
			}
		}
		
		bool semver2::operator <(const cpm::version& o) const {
			if(this->stdIdentifier() == o.stdIdentifier()) {
				const semver2* f = this;
				const semver2* s = (semver2*)&o;
				return f->major < s->major? true :
					f->major > s->major? false :
					f->minor < s->minor? true :
					f->minor > s->minor? false :
					f->patch < s->patch? true : false;
			} else {
				return false;
			}
		}
		
		char* semver2::toCharArray() const {
			if(this->release.length() > 0) {
				if(this->build.length() > 0) {
					return cpm::format("%ju.%ju.%ju-%s+%s", this->major, this->minor, this->patch, this->release.data(), this->build.data());
				} else {
					return cpm::format("%ju.%ju.%ju-%s", this->major, this->minor, this->patch, this->release.data());
				}
			} else {
				if(this->build.length() > 0) {
					return cpm::format("%ju.%ju.%ju+%s", this->major, this->minor, this->patch, this->build.data());
				} else {
					return cpm::format("%ju.%ju.%ju", this->major, this->minor, this->patch);
				}
			}
		}
		
		std::string semver2::toString() const {
			char* part = cpm::format("%ju.%ju.%ju", this->major, this->minor, this->patch);
			std::stringstream s(part);
			std::free(part);
			if(this->release.length() > 0) {
				s << "-" << this->release;
			}
			if(this->build.length() > 0) {
				s << "+" << this->build;
			}
			return s.str();
		}
	}
}
