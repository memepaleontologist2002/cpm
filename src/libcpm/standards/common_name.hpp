#ifndef CPM_STD_COMMON_NAME_HPP
#define CPM_STD_COMMON_NAME_HPP

#include <cerrno>
#include <cstring>

#include <string>

#include "cpm.hpp"

namespace cpm {
	/*!
	 * @brief A form for a common name.
	 * 
	 * A form is a class with just public members which should be set to
	 * appropriate values. Then, they can be converted to another class.
	 * In this case, the class is a @ref common_name.
	 * 
	 * @sa cpm::common_name
	 */
	class common_name_form {
		public:
			/*!
			 * @brief The name.
			 * 
			 * It is as a c style string.
			 * If you have a std::string, set to its .data:
			 * 
			 * 	std::string toconvert = "test";
			 * 	cpm::common_name_form f;
			 * 	f.name = toconvert.data();
			 * 	cpm::common_name(f);
			 */
			const char* name;
	};
	
	
	/*!
	 * @brief A common name standard.
	 * 
	 * It provides a template parameter to specify a function which
	 * should check whether a string matches the name standard.
	 * 
	 * For using this, just do the following:
	 * 
	 * 	typedef common_name<&mcheckfunc> mystd;
	 * 
	 * @tparam check A function which checks whether a string is a
	 *               valid name.
	 */
	template<bool(*check)(const std::string& s)>
	class common_name : public name {
		private:
			/*!
			 * @brief The real name.
			 */
			std::string c;
		public:
			/*!
			 * @copydoc cpm::standardized::stdIdentifier()const
			 */
			virtual const void* stdIdentifier() const override {
				return (const void*)check;
			}
			
			/*!
			 * @brief Initializes an empty name.
			 * 
			 * If a empty name is invalid, throws error_nostdsupport.
			 * 
			 * @throw error_nostdsupport If an empty name is invalid.
			 */
			common_name() : c("") {
				if(!check(c)) {
					throw error_nostdsupport();
				}
			}
			
			/*!
			 * @brief Converts a string to a name.
			 * 
			 * @param n The string to convert.
			 * 
			 * @throw error_nostdsupport If the standard
			 *                           doesn't allow the
			 *                           specified name.
			 */
			common_name(const std::string& n) : c(n) {
				if(!check(c)) {
					throw error_nostdsupport();
				}
			}
			
			/*!
			 * @brief Converts a filled form to a name.
			 * 
			 * It uses the data in the form.
			 * 
			 * @param form The filled form.
			 * 
			 * @throw error_nostdsupport If the specified name
			 *                           is invalid according
			 *                           to the standard.
			 * 
			 * @sa cpm::common_name_form
			 */
			common_name(const common_name_form& form) : c(form.name) {
				if(!check(c)) {
					throw error_nostdsupport();
				}
			}
			
			virtual common_name<check>* clone() const override = 0;
			
			/*!
			 * @brief Compares two common_names.
			 * 
			 * Compares case-sensitive.
			 * 
			 * @return true if both are the same, false otherwise.
			 */
			virtual bool operator==(const name& o) const override {
				if(stdIdentifier() == o.stdIdentifier()) {
					return c == ((common_name<check>*)&o)->c;
				} else {
					return false;
				}
			}
			
			/*!
			 * @brief Converts to a string.
			 * 
			 * This should be used for output.
			 * 
			 * @return The string to print if mentioning this name.
			 */
			char* toCharArray() const override {
				auto errprev = errno;
				char* r = NULL;
				r = strdup(c.data());
				if(r == NULL) {
					goto err;
				}
				return r;
			err:
				switch(errno) {
					case ENOMEM: throw std::bad_alloc();
					default: throw std::exception();
				}
			}
			
			/*!
			 * @brief Converts to a string.
			 * 
			 * This should be used for output.
			 * 
			 * @return The string to print if mentioning this name.
			 */
			std::string toString() const override {
				return c;
			};
	};
}

#endif
