#ifndef CPM_STD_QUICK_NAME_HPP
#define CPM_STD_QUICK_NAME_HPP

#include <cerrno>
#include <cstring>

#include <string>

#include "cpm.hpp"
#include "standards/common_name.hpp"
#include "charset.hpp"

namespace cpm {
	/*!
	 * @brief A common checking function.
	 * 
	 * This is based upon a common concept:
	 * To be valid,
	 * - the first character has to be in a set of characters
	 * - and the second one has to be in another set of characters,
	 *   usually containing more characters.
	 * 
	 * This also has an optional parameter, specifying the minimum length.
	 * 
	 * This is actually a utility function. To use it, return the result of this in the
	 * check() function of your name standard class. In order to make it faster, you
	 * may make all parameters static variables.
	 * 
	 * 	bool check(const std::string& s) {
	 * 		const static charset f = "abc...";
	 * 		const static charset o = "abc...";
	 * 		return quick_name_isValid(s, f, o, 1);
	 * 	}
	 * 
	 * @param s The string to check.
	 * @param first The @ref charset in which the first character should be.
	 * @param second The @ref charset in which all except the first character should be.
	 * @param minlen The minimum length of a name. 1 by default (to forbid empty names).
	 */
	bool quick_name_isValid(const std::string& s, const cpm::charset first, const cpm::charset other, uint_fast8_t minlen = 1);
}

#endif
