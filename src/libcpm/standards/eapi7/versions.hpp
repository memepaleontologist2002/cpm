#ifndef CPM_STD_EAPI7_VERSIONS_HPP
#define CPM_STD_EAPI7_VERSIONS_HPP

#include <vector>
#include <sstream>

#include <gmpxx.h>

#include "cpm.hpp"
#include "util.hpp"

namespace cpm {
	namespace standards {
		namespace eapi7 {
			class version;
			class version_form;
		}
	}
}

namespace cpm {
	namespace standards {
		/*!
		 * @brief The eapi 7 package manager specification.
		 * 
		 * @sa https://wiki.gentoo.org/wiki/Project:Package_Manager_Specification
		 */
		namespace eapi7 {
			/*!
			 * @brief A form for eapi7 versions.
			 * 
			 * Currently not part of the public API.
			 * 
			 * This can convert a string to a version, as this is possible.
			 */
			class version_form {
				public:
					/*!
					 * @brief The string to convert.
					 * 
					 * If you have a std::string instead of a char*, just
					 * save its .data() here and convert it.
					 */
					const char* str;
			};
			
			/*!
			 * @brief An eapi7 version.
			 * 
			 * Currently not part of the public API.
			 */
			class version : public cpm::version {
				public:
					/*!
					 * @brief An eapi7 version suffix.
					 * 
					 * The standard allows suffixes. Each one begins with an underscore, followed by a suffix type and a unsigned unlimted number.
					 * 
					 * Suffix types:
					 * - `_alpha`
					 * - `_beta`
					 * - `_pre`
					 * - `_rc`
					 * - `_p`
					 * 
					 * Examples:
					 * - `_rc4`
					 * - `_pre`
					 */
					class suffix {
						public:
							/*!
							 * @brief A suffix type.
							 */
							enum type {
								_ALPHA, ///< Alpha version
								_BETA, ///< Beta version
								_PRE, ///< Pre release
								_RC, ///< Release candidate
								_P ///< Patch
							} str; ///< The type of this suffix
							
							/*!
							 * @brief The number after the suffix type.
							 */
							mpz_class value = 0;
							
							/*!
							 * @brief Whether the suffix has a value.
							 */
							bool hasvalue = false;
							
							/*!
							 * @brief Convert this suffix to a string.
							 */
							operator std::string() const {
								std::stringstream r;
								switch (str) {
									case _ALPHA:
										r << "_alpha";
										break;
									case _BETA:
										r << "_beta";
										break;
									case _PRE:
										r << "_pre";
										break;
									case _RC:
										r << "_rc";
										break;
									case _P:
										r << "_p";
										break;
								}
								// Append value if one is there
								if(hasvalue) {
									r << value;
								}
								return r.str();
							}
					};
					
					/*!
					 * @brief The numbers in the number part.
					 *
					 * The package manager specification specifies a version starts with
					 * a number part. It consists out of numbers separated by single dots.
					 * All of these numbers have to be unsigned.
					 * 
					 * This contains these numbers, in order.
					 */
					std::vector<nullprefixeduint> numberpart;
					
					/*!
					 * @brief The version sufixes.
					 * 
					 * The package manager specification specifies that a version can have
					 * sufixes.
					 * 
					 * @sa cpm::standards::eapi7::version::suffix
					 */
					std::vector<suffix> sufixes;
					
					/*!
					 * @brief The single letter.
					 * 
					 * The package manager specification specifies there can be a
					 * single letter between the number part and the sufixes. It
					 * is saved here.
					 * 
					 * If here the null character is saved, there is no character.
					 */
					char c = '\0';
					
					/*!
					 * @brief The revision number.
					 * 
					 * The package manager specification specifies that after the sufixes,
					 * `-r<number>` can be added to the end. If none is given, 0 is assumed.
					 */
					mpz_class revision = 0;
					
					/*!
					 * @brief Saves whether originally there was a revision number.
					 */
					bool hasrevision = false;
					
					/*!
					 * @brief Default constructor.
					 * 
					 * Initializes invalid version.
					 */
					version();
					
					/*!
					 * @brief Converts a filled version form to an actual version.
					 * 
					 * @sa version_form
					 */
					version(const version_form f);
					
					virtual version* clone() const final override;
					
					const void* stdIdentifier() const final override;
					
					/*!
					 * @brief Converts this version to a string.
					 */
					operator std::string() const;
					
					/*!
					 * @brief Converts this version to a string.
					 * 
					 * The memory has to be freed by the caller.
					 */
					operator char*() const;
					bool operator> (const cpm::version& o) const final override;
					bool operator==(const cpm::version& o) const final override;
					bool operator< (const cpm::version& o) const final override;
			};
		}
	}
}


#endif
