#include "names.hpp"

#include <cstdint>

#include <string>

#include "cpm.hpp"

#include "charset.hpp"
#include "versions.hpp"
#include "standards/quick_name.hpp"

namespace cpm {
	namespace standards {
		namespace eapi7 {
			bool category_name_check(const std::string &s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+_.-";
				return quick_name_isValid(s, a, b, 1);
			}
			
			bool package_name_check(const std::string &s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+_-";
				if(!(quick_name_isValid(s, a, b))) {
					return false;
				}
				cpm::standards::eapi7::version_form f;
				for(const char* c = s.data(); *c != '\0'; ++c) {
					if(*c == '-') {
						f.str = c+1;
						try {
							cpm::standards::eapi7::version _ = f;
						} catch(cpm::error_nostdsupport) {
							continue;
						}
						return false;
					}
				}
				return true;
			}
			
			bool slot_name_check(const std::string &s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+_.-";
				return quick_name_isValid(s, a, b, 1);
			}
			
			bool use_name_check(const std::string &s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+_@-";
				return quick_name_isValid(s, a, b, 1);
			}
			
			bool repo_name_check(const std::string &s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-";
				return quick_name_isValid(s, a, b, 1) || !package_name_check(s);
			}
			
			bool license_name_check(const std::string &s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+_.-";
				return quick_name_isValid(s, a, b, 1);
			}
			
			bool branch_name_check(const std::string &s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-";
				return quick_name_isValid(s, a, b, 1) || !package_name_check(s);
			}
			
			bool eapi_name_check(const std::string &s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+_.-";
				return quick_name_isValid(s, a, b, 1);
			}
			
			//
			// clone() methods
			category_name* category_name::clone() const {
				return new category_name(*this);
			}
			
			package_name* package_name::clone() const {
				return new package_name(*this);
			}
			
			slot_name* slot_name::clone() const {
				return new slot_name(*this);
			}
			
			use_name* use_name::clone() const {
				return new use_name(*this);
			}
			
			repo_name* repo_name::clone() const {
				return new repo_name(*this);
			}
			
			license_name* license_name::clone() const {
				return new license_name(*this);
			}
			
			branch_name* branch_name::clone() const {
				return new branch_name(*this);
			}
			
			eapi_name* eapi_name::clone() const {
				return new eapi_name(*this);
			}
		}
	}
}
