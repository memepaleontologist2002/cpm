#include "pkgnodes.hpp"
#include "standards/common_pkgnode.hpp"


namespace {
	// value is never read, just address
	const static bool eapi7addr = false;
}

namespace cpm {
	namespace standards {
		namespace eapi7 {
			const bool repository::repoStandard = false;
			repository* repository::clone() const {
				return new repository(*this);
			};
			void repository::initialize_children() const {
				if(uninitialized[CHILDREN]){
					for(const std::filesystem::directory_entry& ent : std::filesystem::directory_iterator(local_files)) {
						if(ent.is_directory()) {
							std::filesystem::path p = ent.path();
							auto i = p.end();
							--i;
							if(i->empty()) {
								--i;
							}
							const std::string n = *i;
							if(isCategory(n)) {
								children.push_back(category(this, n));
							}
						}
					}
				}
			};
			bool repository::isCategory(const std::string& n) const {
				if(n.size() == 0) {
					return false;
				} else if(n[0] == '.') {
					return false;
				} else if(n == dir_profiles) {
					return false;
				} else if(n == dir_licenses) {
					return false;
				} else if(n == dir_eclass) {
					return false;
				} else if(n == dir_metadata) {
					return false;
				} else {
					return true;
				}
			}
			repository::repository() : cpm::common_repository(new repo_name("")) {}
			repository::repository(const repository_form& form) :
				cpm::common_repository(form.nameUserPreferred),
				dir_profiles(form.dir_profiles),
				dir_licenses(form.dir_licenses),
				dir_eclass(form.dir_eclass),
				dir_metadata(form.dir_metadata),
				local_files(form.location)
			{}
			const void* repository::stdIdentifier() const {
				return &repoStandard;
			}
			bool repository::operator==(const pkgnode& other) const {
				if(stdIdentifier() == other.stdIdentifier()) {
					return *this == *(repository*)&other;
				} else {
					return false;
				}
			}
			bool repository::operator==(const repository& other) const {
				return this->local_files == other.local_files && *this->getNameUserPreferred() == *other.getNameUserPreferred();
			}
			const pkgnode* repository::getParent() const {
				return NULL;
			}
			std::vector<cpm::pkgnode*> repository::getChildren() {
				initialize_children();
				std::vector<cpm::pkgnode*> r;
				r.reserve(children.size());
				for(category i : children) {
					r.push_back(&i);
				}
				return r;
			}
			std::vector<const cpm::pkgnode*> repository::getChildren() const {
				initialize_children();
				std::vector<const cpm::pkgnode*> r;
				r.reserve(children.size());
				for(category i : children) {
					r.push_back(&i);
				}
				return r;
			}
			std::vector<cpm::package*> repository::getPackages() {
				return std::vector<cpm::package*>();
			}
			std::vector<const cpm::package*> repository::getPackages() const {
				return std::vector<const cpm::package*>();
			}
			
			category* category::clone() const {
				return new category(*this);
			}
			const bool category::categoryStandard = false;
			void category::initialize_packages() const {
				if(uninitialized[PACKAGES]){
					for(const std::filesystem::directory_entry& ent : std::filesystem::directory_iterator(top->local_files/(std::string)*getNameUserPreferred())) {
						if(ent.is_directory()) {
							std::filesystem::path p = ent.path();
							auto i = p.end();
							--i;
							if(i->empty()) {
								--i;
							}
							const std::string n = *i;
							if(isPackage(n)) {
								packages.push_back(package(this, package_name(n)));
							}
						}
					}
				}
			};
			bool category::isPackage(const std::string& n) const {
				if(n.size() == 0) {
					return false;
				} else if(n[0] == '.') {
					return false;
				} else if(n == "CVS") {
					return false;
				} else {
					return true;
				}
			}
			category::category(const repository* repo, const category_name& n) : top(repo), common_pkgnode(&n) {}
			const void* category::stdIdentifier() const {
				return &categoryStandard;
			}
			bool category::operator==(const cpm::pkgnode& other) const {
				if(this->stdIdentifier() == other.stdIdentifier()) {
					return *this == *(category*)&other;
				} else {
					return false;
				}
			}
			bool category::operator==(const category& other) const {
				return *this->top == *other.top && this->getNameUserPreferred() == other.getNameUserPreferred();
			}
			std::vector<cpm::pkgnode*> category::getChildren() {
				return std::vector<cpm::pkgnode*>();
			}
			std::vector<const cpm::pkgnode*> category::getChildren() const {
				return std::vector<const cpm::pkgnode*>();
			}
			std::vector<cpm::package*> category::getPackages() {
				initialize_packages();
				std::vector<cpm::package*> r;
				r.reserve(packages.size());
				for(package& p : packages) {
					r.push_back((cpm::package*)&p);
				}
				return r;
			}
			std::vector<const cpm::package*> category::getPackages() const {
				initialize_packages();
				std::vector<const cpm::package*> r;
				r.reserve(packages.size());
				for(package& p : packages) {
					r.push_back((const cpm::package*)&p);
				}
				return r;
			}
			const pkgnode* category::getParent() const {
				return top;
			}
			
			// package
			package* package::clone() const {
				return new package(*this);
			}
			package::package(const category* c, const package_name& n) : cat(c), name(n) {}
			package::~package() {}
			const void* package::stdIdentifier() const {
				return &eapi7addr;
			}
		}
	}
}
