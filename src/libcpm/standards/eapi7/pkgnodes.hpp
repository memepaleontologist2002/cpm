#ifndef CPM_EAPI7_PKGNODES_HPP
#define CPM_EAPI7_PKGNODES_HPP

#include <bitset>
#include <filesystem>

#include "cpm.hpp"
#include "standards/common_pkgnode.hpp"
#include "names.hpp"

namespace cpm {
	namespace standards {
		namespace eapi7 {
			class repository;
			class category;
			class package;
			
			class repository_form;
			
			/*!
			 * @brief A form for an eapi7 repository.
			 * 
			 * @sa cpm::standards::eapi7::repository
			 */
			class repository_form {
				public:
					/*!
					 * @brief The special directory for profiles.
					 * 
					 * Should always be profiles.
					 */
					std::string dir_profiles = "profiles";
					
					/*!
					 * @brief The special directory for licenses.
					 * 
					 * Should always be licenses.
					 */
					std::string dir_licenses = "licenses";
					
					/*!
					 * @brief The special directory for eclasses.
					 * 
					 * Should always be eclass.
					 */
					std::string dir_eclass = "eclass";
					
					/*!
					 * @brief The special directory for metadata.
					 * 
					 * Should always be metadata.
					 */
					std::string dir_metadata = "metadata";
					
					/*!
					 * @brief Where the data is stored locally.
					 */
					std::filesystem::path location;
					
					/*!
					 * @brief The user preferred name.
					 */
					name* nameUserPreferred;
			};
			
			/*!
			 * @brief An eapi7 repository.
			 */
			class repository : public cpm::common_repository {
				public:
					virtual repository* clone() const final override;
					
				private:
					/*!
					 * @brief Address for stdIdentifier.
					 * 
					 * @sa stdIdentifier()const
					 */
					const static bool repoStandard;
					
					/*!
					 * @brief The special directory for profiles.
					 * 
					 * Should always be profiles.
					 */
					std::string dir_profiles = "profiles";
					
					/*!
					 * @brief The special directory for licenses.
					 * 
					 * Should always be licenses.
					 */
					std::string dir_licenses = "licenses";
					
					/*!
					 * @brief The special directory for eclasses.
					 * 
					 * Should always be eclass.
					 */
					std::string dir_eclass = "eclass";
					
					/*!
					 * @brief The special directory for metadata.
					 * 
					 * Should always be metadata.
					 */
					std::string dir_metadata = "metadata";
					
					/*!
					 * @brief Anti-alaysing fields.
					 */
					enum fields : std::uint8_t {
						CHILDREN,
						MAX = CHILDREN
					};
					
					/*!
					 * @brief Whether a @ref fields is evaluated.
					 */
					mutable cpm::flags<fields, fields::MAX> uninitialized;
					
					/*!
					 * @brief The categories in this repository.
					 */
					mutable std::vector<category> children;
					
					/*!
					 * @brief Initialize @ref children.
					 */
					void initialize_children() const;
					
					/*!
					 * @brief Checks whether a child directory is a category directory.
					 * 
					 * It implies that n is the name of a subdirectory of @ref local_files.
					 * 
					 * @param n The name to check.
					 * 
					 * @return Whether n is a category directory.
					 */
					bool isCategory(const std::string& n) const;
				public:
					/*!
					 * @brief Where the data is stored locally.
					 */
					std::filesystem::path local_files;
					
					repository();
					
					/*!
					 * @brief Create a repository out of a filled form.
					 */
					repository(const repository_form& f);
					
					const void* stdIdentifier() const override;
					
					bool operator==(const cpm::pkgnode& other) const final override;
					bool operator==(const repository& other) const;
					
					std::vector<cpm::pkgnode*> getChildren() override;
					
					std::vector<const cpm::pkgnode*> getChildren() const override;
					
					std::vector<cpm::package*> getPackages() override;
					
					std::vector<const cpm::package*> getPackages() const override;
					
					const pkgnode* getParent() const override;
			};
			
			/*!
			 * @brief An eapi7 package category.
			 */
			class category : public cpm::common_pkgnode {
				public:
					virtual category* clone() const final override;
					
				private:
					/*!
					 * @brief Address for stdIdentifier.
					 * 
					 * @sa stdIdentifier()const
					 */
					const static bool categoryStandard;
					
					category(const repository* repo, const category_name& n);
					
					/*!
					 * @brief Anti-alaysing fields.
					 */
					enum fields : std::uint8_t {
						PACKAGES,
						MAX = PACKAGES
					};
					
					/*!
					 * @brief Whether a @ref fields is evaluated.
					 */
					mutable cpm::flags<fields, fields::MAX> uninitialized;
					
					/*!
					 * @brief The packages in this @ref category.
					 */
					mutable std::vector<package> packages;
					
					/*!
					 * @brief Initialize @ref packages.
					 */
					void initialize_packages() const;
					
					/*!
					 * @brief Checks whether a child directory is a package directory.
					 * 
					 * It implies that n is the name of a subdirectory of the category
					 * directory (`top->local_files / name`).
					 * 
					 * @param n The name to check.
					 * 
					 * @return Whether n is a package directory.
					 */
					bool isPackage(const std::string& n) const;
					
					friend repository;
				public:
					/*!
					 * @brief The repository containing this category.
					 */
					const repository* top;
					
					const void* stdIdentifier() const override;
					
					bool operator==(const cpm::pkgnode& other) const final override;
					bool operator==(const category& other) const;
					
					std::vector<cpm::pkgnode*> getChildren() override;
					std::vector<const cpm::pkgnode*> getChildren() const override;
					
					std::vector<cpm::package*> getPackages() override;
					std::vector<const cpm::package*> getPackages() const override;
					
					const pkgnode* getParent() const override;
			};
			
			/*!
			 * @brief An eapi7 package.
			 */
			class package : public cpm::package {
				public:
					virtual package* clone() const final override;
					
				private:
					package(const category* c, const package_name& n);
					
					friend category;
				public:
					/*!
					 * @brief The category containing this package.
					 */
					const category* cat;
					
					/*!
					 * @brief Package name.
					 */
					package_name name;
					
					const void* stdIdentifier() const override;
					
					~package();
			};
		}
	}
}

#endif
