#include "versions.hpp"

#include <cstdint>

#include <string>
#include <sstream>

#include <gmpxx.h>

#include "cpm.hpp"
#include "util.hpp"

namespace {
	enum result : uint_fast8_t {
		SMALLER,
		EQUAL,
		GREATER
	};
	
	// All of the following algorithms are translated directly from the specification
	result alg7(const cpm::standards::eapi7::version* first, const cpm::standards::eapi7::version* second) {
		mpz_class ar = first->hasrevision? first->revision : 0;
		mpz_class br = second->hasrevision? second->revision : 0;
		if(ar > br) {
			return GREATER;
		} else if(ar < br) {
			return SMALLER;
		}
		return EQUAL;
	}
	result alg6(const cpm::standards::eapi7::version::suffix& a, const cpm::standards::eapi7::version::suffix& b) {
		if(a.str == b.str) {
			mpz_class asi = a.hasvalue? a.value : 0;
			mpz_class bsi = b.hasvalue? b.value : 0;
			if(asi > bsi) {
				return GREATER;
			} else if(asi < bsi) {
				return SMALLER;
			}
		} else if(a.str > b.str) {
			return GREATER;
		} else if(a.str < b.str) {
			return SMALLER;
		}
		return EQUAL;
	}
	result alg5(const cpm::standards::eapi7::version* first, const cpm::standards::eapi7::version* second) {
		std::size_t asn = first->sufixes.size();
		std::size_t bsn = second->sufixes.size();
		result state;
		for(std::size_t i = 0; i < asn && i < bsn; ++i) {
			if((state = alg6(first->sufixes[i], second->sufixes[i])) != EQUAL) {
				return state;
			}
		}
		if(asn > bsn) {
			if(first->sufixes.back().str == cpm::standards::eapi7::version::suffix::_P) {
				return GREATER;
			} else {
				return SMALLER;
			}
		} else if(asn < bsn) {
			if(second->sufixes.back().str == cpm::standards::eapi7::version::suffix::_P) {
				return SMALLER;
			} else {
				return GREATER;
			}
		}
		return EQUAL;
	}
	result alg4(const cpm::standards::eapi7::version* first, const cpm::standards::eapi7::version* second) {
		if(first->c > second->c) {
			return GREATER;
		} else if(first->c < second->c) {
			return SMALLER;
		}
		return EQUAL;
	}
	result alg3(const cpm::nullprefixeduint& f, const cpm::nullprefixeduint& o) {
		if(f.zeros > 0 || o.zeros > 0) {
			cpm::nullprefixeduint a = f;
			cpm::nullprefixeduint b = o;
			while(a.value % 10 == 0) {
				a.value /= 10;
			}
			while(b.value % 10 == 0) {
				b.value /= 10;
			}
			std::string s = a;
			std::string t = b;
			
			std::int_fast8_t c = cpm::strasciicmp(s.data(), t.data());
			if(c > 0) {
				return GREATER;
			} else if(c < 0) {
				return SMALLER;
			}
		} else {
			if(f > o) {
				return GREATER;
			} else if(f < o) {
				return SMALLER;
			}
		}
		return EQUAL;
	}
	result alg2(const cpm::standards::eapi7::version* first, const cpm::standards::eapi7::version* second) {
		if(first->numberpart[0] > second->numberpart[0]) {
			return GREATER;
		} else if(first->numberpart[0] < second->numberpart[0]) {
			return SMALLER;
		}
		std::size_t ann = first->numberpart.size();
		std::size_t bnn = second->numberpart.size();
		result state;
		for(std::size_t i = 1; i < ann && i < bnn; ++i) {
			if((state = alg3(first->numberpart[i], second->numberpart[i])) != EQUAL) {
				return state;
			}
		}
		if(ann > bnn) {
			return GREATER;
		} else if(ann < bnn) {
			return SMALLER;
		}
		return EQUAL;
	}
	result alg1(const cpm::standards::eapi7::version* first, const cpm::standards::eapi7::version* second) {
		result state = EQUAL;
#define alg_(n) if((state = alg ## n (first, second)) != EQUAL) {return state;}
		alg_(2);
		alg_(4);
		alg_(5);
		alg_(7);
#undef alg_
		return EQUAL;
	}
}

namespace cpm {
	namespace standards {
		namespace eapi7 {
			version::version() {}
			
			version::version(const version_form f) {
				const char* at = f.str;
				while(true) {
					const char* atbefore = at;
					numberpart.emplace_back(at);
					if(*at == '.') {
						++at;
					} else {
						break;
					}
				}
				while(*at == '_') {
					suffix s;
					switch(*++at) {
						case 'a':
							if(strprefix_mv(at, "alpha")) {
								s.str = suffix::_ALPHA;
							} else {
								throw error_nostdsupport();
							}
							break;
						case 'b':
							if(strprefix_mv(at, "beta")) {
								s.str = suffix::_BETA;
							} else {
								throw error_nostdsupport();
							}
							break;
						case 'p':
							if(strprefix_mv(at, "pre")) {
								s.str = suffix::_PRE;
							} else {
								// _p alone is a valid suffix
								++at;
								s.str = suffix::_P;
							}
							break;
						case 'r':
							if(strprefix_mv(at, "rc")) {
								s.str = suffix::_RC;
							} else {
								throw error_nostdsupport();
							}
							break;
						default:
							throw error_nostdsupport();
					}
					const char* atbefore = at;
					s.value = strtompz(at);
					if(at == atbefore) {
						s.hasvalue = false;
						s.value = 0;
					} else {
						s.hasvalue = true;
					}
					sufixes.emplace_back(std::move(s));
				}
				hasrevision = false;
				if(*at == '-') {
					if(*++at == 'r') {
						++at;
						hasrevision = true;
						revision = strtompz(at);
					}
				}
				if(*at != '\0') {
					throw error_nostdsupport();
				}
			}
			
			version* version::clone() const {
				return new version(*this);
			}
			
			const void* version::stdIdentifier() const {
				static bool a;
				return &a;
			};
			
			version::operator std::string() const {
				std::stringstream r;
				for(size_t i = 0; i < numberpart.size(); ++i) {
					r << numberpart[i];
					if(i + 1 < numberpart.size()) {
						r << ".";
					}
				}
				if(c != '\0') {
					r << c;
				}
				for(const suffix& i : sufixes) {
					r << (std::string) i;
				}
				if(hasrevision) {
					r << "-r" << revision;
				}
				return r.str();
			}
			version::operator char*() const {
				char* r = NULL;
				std::string s = *this;
				r = strdup(s.data());
				if(r == NULL) {
					goto err;
				}
				return r;
			err:
				switch(errno) {
					case ENOMEM: throw std::bad_alloc();
					default: throw std::exception();
				}
			}
			
			bool version::operator< (const cpm::version& o) const {
				if(stdIdentifier() != o.stdIdentifier()) {
					return false;
				}
				return alg1(this, (eapi7::version*)&o) == SMALLER;
			}
			
			bool version::operator==(const cpm::version& o) const {
				if(stdIdentifier() != o.stdIdentifier()) {
					return false;
				}
				return alg1(this, (eapi7::version*)&o) == EQUAL;
			}
			
			bool version::operator> (const cpm::version& o) const {
				if(stdIdentifier() != o.stdIdentifier()) {
					return false;
				}
				return alg1(this, (eapi7::version*)&o) == GREATER;
			}
		}
	}
}
