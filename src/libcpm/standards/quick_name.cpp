#include "quick_name.hpp"

#include <cstdint>

#include <string>

#include "charset.hpp"

namespace cpm {
	bool quick_name_isValid(const std::string& s, const cpm::charset first, const cpm::charset other, uint_fast8_t minlen) {
		if(s.size() < minlen) {
			return false;
		}
		if(s.size() > 1) {
			return first.contains(s[0]) && other.contains(s.data() + 1);
		} else if(s.size() == 1) {
			return first.contains(s[0]);
		} else {
			return true;
		}
	}
}
