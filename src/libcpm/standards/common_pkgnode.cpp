#include "common_pkgnode.hpp"

namespace cpm {
	common_repository::common_repository(const name* nameUserPreferred) : nameUserPreferred(nameUserPreferred->clone()) {}
	const name* common_repository::getNameUserPreferred() const {
		return nameUserPreferred;
	}
	
	common_pkgnode::common_pkgnode(const name* nameUserPreferred) : nameUserPreferred(nameUserPreferred->clone()) {}
	const name* common_pkgnode::getNameUserPreferred() const {
		return nameUserPreferred;
	}
}
