#ifndef CPM_SEMVER2_HPP
#define CPM_SEMVER2_HPP

#include "cpm.hpp"

namespace cpm {
	namespace standards {
		class semver2;
		
		/*!
		 * @brief A container which contains the data for a semantic version 2.
		 * 
		 * Just used internally, **DO NOT USE ELSEWHERE**!
		 * 
		 * @sa https://semver.org
		 */
		class semver2 : public version {
			public:
				/*!
				 * @brief The major version.
				 * 
				 * @sa https://semver.org
				 */
				std::uintmax_t major;
				
				/*!
				 * @brief The minor version.
				 * 
				 * @sa https://semver.org
				 */
				std::uintmax_t minor;
				
				/*!
				 * @brief The patch version.
				 * 
				 * @sa https://semver.org
				 */
				std::uintmax_t patch;
				
				/*!
				 * @brief The release information.
				 * 
				 * If empty, none is there.
				 * 
				 * @sa https://semver.org
				 */
				std::string release;
				
				/*!
				 * @brief The build information.
				 * 
				 * If empty, none is there.
				 * 
				 * @sa https://semver.org
				 */
				std::string build;
				
				virtual const void* stdIdentifier() const override;
				
				virtual bool operator==(const cpm::version& o) const override;
				
				virtual bool operator >(const cpm::version& o) const override;
				
				virtual bool operator <(const cpm::version& o) const override;
				
				/*!
				 * @brief Convert to a human-readable string.
				 * 
				 * Should be used for output.
				 */
				char* toCharArray() const;
				
				/*!
				 * @copydoc cpm::standards::semver2::toCharArray()const
				 */
				std::string toString() const;
		};
	}
}

#endif
