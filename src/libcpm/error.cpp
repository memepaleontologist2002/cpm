#include "cpm.hpp"

namespace cpm {
	const char* error::what() const noexcept {
		return "An exception happened in libcpm.";
	}
	const char* error_format::what() const noexcept {
		return "An error occurred at formatting a format string.";
	}
	const char* error_nostdsupport::what() const noexcept {
		return "The standard doesn't support this operation.";
	}
	const char* error_file_open::what() const noexcept {
		return "Could not open file.";
	}
	const char* error_file_format::what() const noexcept {
		return "No file parser has succeeded.";
	}
}

