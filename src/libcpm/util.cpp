#include <cstring>

#include <string>
#include <sstream>

#include "util.hpp"

namespace cpm {
	bool strprefix(const char* first, const char* second) {
		return std::memcmp(first, second, std::strlen(second)) == 0;
	}
	
	bool strprefix_mv(const char*& first, const char *second) {
		size_t len = std::strlen(second);
		if(std::memcmp(first, second, len) == 0) {
			first += len;
			return true;
		} else {
			return false;
		}
	}
	
	std::int_fast8_t strasciicmp(const char *first, const char *second) {
		while(*first != '\0' && *second != '\0') {
			if(*first != *second) {
				if(*first < *second) {
					return -1;
				} else {
					return 1;
				}
			}
			first++;
			second++;
		}
		// At least one of *first and *second is '\0'
		if(*first != *second) {
			if(*first == '\0') {
				return -1;
			} else {
				return 1;
			}
		}
		return 0;
	}
	
	mpz_class strtompz(const char*& str) {
		std::size_t len = 0;
		const char* at = str;
		while (*at >= '0' && *at <= '9') {
			++at;
			++len;
		}
		if(len == 0) {
			return 0;
		}
		char* s = NULL;
		s = new char[len + 1 /* \0 */];
		s[len] = '\0';
		memcpy(s, str, len);
		mpz_class r(s);
		str = at;
		return r;
	}
	
	nullprefixeduint::nullprefixeduint(const char*& o) {
		const char* at = o;
		while(*at == '0') {
			++zeros;
			++at;
		}
		std::size_t len = 0;
		const char* newat = at;
		while(*newat >= '0' && *newat <= '9') {
			++len;
			++newat;
		}
		if(len == 0) {
			value = 0;
		} else {
			char* num = NULL;
			num = new char[len + 1 /* \0 */];
			num[len] = '\0';
			std::memcpy(num, at, len);
			value = num;
			std::free(num);
		}
		o = newat;
	}
	
	nullprefixeduint::operator std::string() const {
		std::stringstream r;
		for(mpz_class at = 0; at < zeros; ++at) {
			r << '0';
		}
		r << value.get_str();
		return r.str();
	}
	
	bool nullprefixeduint::operator>(const nullprefixeduint& o) const {
		return value > o.value;
	}
	bool nullprefixeduint::operator<(const nullprefixeduint& o) const {
		return value < o.value;
	}
	bool nullprefixeduint::operator==(const nullprefixeduint& o) const {
		return value == o.value;
	}
	bool nullprefixeduint::operator!=(const nullprefixeduint& o) const {
		return value != o.value;
	}
	bool nullprefixeduint::operator>=(const nullprefixeduint& o) const {
		return value >= o.value;
	}
	bool nullprefixeduint::operator<=(const nullprefixeduint& o) const {
		return value <= o.value;
	}
}

std::ostream& operator<<(std::ostream& s, const cpm::nullprefixeduint& n) {
	for(mpz_class i = 0; i < n.zeros; ++i) {
		s << "0";
	}
	s << n.value;
	return s;
}
