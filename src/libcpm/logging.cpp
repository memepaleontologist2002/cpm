#include "cpm.hpp"
#include "logging.hpp"

#include <string>
#include <vector>
#include <algorithm>

namespace cpm {
	log::log(context& c, const std::string& name) : ctx(c) {
		c.backtrace.push_back(name);
	}
	log::log(context& c, std::string&& name) : ctx(c) {
		c.backtrace.push_back(std::move(name));
	}
	log::~log() {
		(void) ctx.backtrace.pop_back();
	}
}
