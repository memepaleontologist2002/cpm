/*!
 * @file
 * @brief Contains the full puplic API of libcpm.
 */

#ifndef CPM_HPP
#define CPM_HPP

#include <cstdint>
#include <ctime>

#include <bitset>
#include <exception>
#include <vector>
#include <string>
#include <istream>
#include <functional>

#ifdef BUILDING_LIBCPM
# ifdef HAVE_CONFIG_H
#  include "config.h"
# endif
# ifdef HAVE_VISIBILITY
#  define LIBCPM_EXPORT __attribute__((__visibility__("default")))
# else
#  define LIBCPM_EXPORT
# endif
#else
# define LIBCPM_EXPORT
#endif

namespace cpm {
	class LIBCPM_EXPORT error;
	class LIBCPM_EXPORT error_format;
	class LIBCPM_EXPORT error_nostdsupport;
	class LIBCPM_EXPORT error_file_open;
	class LIBCPM_EXPORT error_file_format;
	
	// A class which I thought was missing, so I made it myself
	template<class _T, std::size_t max>
	class LIBCPM_EXPORT flags;
	
	class LIBCPM_EXPORT context;
	class LIBCPM_EXPORT configuration;
	
	class LIBCPM_EXPORT standardized;
	
	class LIBCPM_EXPORT name;
	class LIBCPM_EXPORT version;
	
	class LIBCPM_EXPORT importance;
	template<class result>
	class LIBCPM_EXPORT file_parser;
	class LIBCPM_EXPORT file_parser_correctness;
	class LIBCPM_EXPORT file_parser_configuration;
	
	class LIBCPM_EXPORT pkgnode;
	class LIBCPM_EXPORT repository;
	class LIBCPM_EXPORT package;
	
	/*!
	 * @brief A logging level.
	 * 
	 * A logging level is an importance or type of logging
	 * messages. Each member is a logging level, take them
	 * as an example.
	 * 
	 * The most commonly used logging levels are (in order
	 * of importance):
	 * 1. error: something making the requested action
	 *    impossible happened (for example, if you tried to
	 *    read a file and you can't open it)
	 * 2. warning: something bad happened, but the action
	 *    is still possible. However, this may lead to
	 *    future errors. For example, if you can't read a
	 *    configuration file, it is still possible to
	 *    install a package, except if the repository of
	 *    that package was defined inside it, which will
	 *    run to an error because of a missing package.
	 * 3. (not as common) notice: A unusual scenario,
	 *    but nothing to worry about. This could be, for a
	 *    not too good example, that the `$PATH` doesn't
	 *    contain `/usr/bin`.
	 * 4. info: Just information which may be useful for
	 *    the user that he maybe should read. This could
	 *    be, for example, the requested action.
	 * 5. debug: Debugging information for the developers
	 *    of a project. This could, for example, be a
	 *    note that a value was read out of a
	 *    configuration file.
	 */
	enum class logging_level : std::int8_t {
		DEBUG, ///< @brief Debugging information.
		INFO, ///< @brief Informations about the things the application tries to do.
		NOTICE, ///< @brief Unusual circumstances, but nothing to worry about.
		WARN, ///< @brief Some circumstances may leading to an error.
		ERROR, ////< @brief An unrecoverable error.
		CONF_COMPLICATED, ///< @brief A complicated configuration.
		CONF_RARE, ///< @brief Rare configuration file syntax.
		CONF_UNLIKELY, ///< @brief Something else would make more sense.
		CONF_WARN, ///< @brief Unreadable line of configuration file.
		CONF_SYNTAX_ERROR, ///< @brief Have to stop because of unrecoverable syntax error (or similar).
		MAX=CONF_SYNTAX_ERROR ///< @brief The maximum value this enum can reach.
	};
	
	/*!
	 * @brief A logging mask.
	 * 
	 * A logging mask specifies which logging_level should be saved.
	 * 
	 * @sa cpm::logging_level
	 */
	typedef flags<logging_level, (std::size_t)logging_level::MAX> logging_levels;
	
	/*!
	 * @brief A logging message.
	 * 
	 * It saves the actual message along with some metadata.
	 */
	class logging_message;
}

namespace cpm {
	/*!
	 * @brief A general error in libcpm happened.
	 * 
	 * This should not be thrown directly; the right subclass should be used instead.
	 */
	class error : public std::exception {
		public:
			/*!
			 * @brief Human-readable description of the error.
			 */
			virtual const char* what() const noexcept;
	};
	
	/*!
	 * @brief An error occurred at formatting.
	 * 
	 * Maybe the format string is not quite right.
	 */
	class error_format : public error {
		public:
			/*!
			 * @brief Human-readable description of the error.
			 */
			virtual const char* what() const noexcept;
	};
	
	/*!
	 * @brief A standard doesn't support this operation.
	 */
	class error_nostdsupport : public error {
		public:
			/*!
			 * @brief Human-readable description of the error.
			 */
			virtual const char* what() const noexcept;
	};
	
	/*!
	 * @brief Couldn't open a file.
	 */
	class error_file_open : public error {
		public:
			/*!
			 * @brief Human-readable description of the error.
			 */
			virtual const char* what() const noexcept;
	};
	
	/*!
	 * @brief Couldn't detect file format.
	 * 
	 * All parsers were tried and none succeeded.
	 */
	class error_file_format : public error {
		public:
			/*!
			 * @brief Human-readable description of the error.
			 */
			virtual const char* what() const noexcept;
	};
	
	/*!
	 * @brief An importance useful for letting something being overwritten.
	 * 
	 * It is like a value, but there is always something in between.
	 * 
	 * I may write it in a form such as `upper lower upper upper lower` ...
	 * 
	 * If you want a thing that is more important than another, append upper.
	 * If you want a thing that is less important than another, append lower.
	 * 
	 * In a function which should return an importance, you can create one
	 * in several ways:
	 * 
	 * 	importance a = "upper lower";
	 * 	importance b;
	 * 	b.upper().lower().lower();
	 * 	importance c = "+--+"; // upper lower lower upper
	 * 
	 * Usually, if an importance starts with lower, it should be ignored.
	 */
	class importance {
		public:
			/*!
			 * @brief Creates an empty importance.
			 * 
			 * It is between `lower` and `upper`.
			 */
			importance();
			
			/*!
			 * @brief Converts suffixes to an importance.
			 * 
			 * Although no separator is required, any non-alphabetic
			 * character except `+`, `-`, `0` and `1` will work. Separators
			 * are skipped. They also can appear at the beginning.
			 * 
			 * `+`, `-`, `0` and `1` can replace `upper` and `lower`:
			 * - `+`: `upper`
			 * - `-`: `lower`
			 * - `1`: `upper`
			 * - `0`: `lower`
			 * 
			 * `upper`and `lower` are compared case-sensitive.
			 * 
			 * @param s The string to convert.
			 */
			importance(const char* s);
			
			/*!
			 * @brief Lowers the importance.
			 * 
			 * Appends `lower` to the value.
			 * 
			 * @return A reference to this.
			 */
			importance& lower();
			/*!
			 * @brief Makes the importance more important.
			 * 
			 * Appends `upper` to the value.
			 * 
			 * @return A reference to this.
			 */
			importance& upper();
			
			/*!
			 * @brief Compares two importances.
			 * 
			 * Higher is more important.
			 */
			bool operator<(const importance& o) const;
			/*!
			 * @brief Compares two importances.
			 * 
			 * Higher is more important.
			 */
			bool operator>(const importance& o) const;
			/*!
			 * @brief Compares two importances.
			 * 
			 * Higher is more important.
			 */
			bool operator==(const importance& o) const;
			/*!
			 * @brief Compares two importances.
			 * 
			 * Higher is more important.
			 */
			bool operator!=(const importance& o) const;
			/*!
			 * @brief Compares two importances.
			 * 
			 * Higher is more important.
			 */
			bool operator<=(const importance& o) const;
			/*!
			 * @brief Compares two importances.
			 * 
			 * Higher is more important.
			 */
			bool operator>=(const importance& o) const;
			
			/*!
			 * @brief Makes the importance more important.
			 * 
			 * Appends `upper` to the value.
			 * 
			 * @return A reference to this.
			 * 
			 * @sa cpm::importance::upper
			 */
			importance& operator++(int);
			/*!
			 * @brief Lowers the importance.
			 * 
			 * Appends `lower` to the value.
			 * 
			 * @return A reference to this.
			 * 
			 * @sa cpm::importance::lower
			 */
			importance& operator--(int);
			/*!
			 * @brief Makes the importance more important.
			 * 
			 * Appends `upper` to the value.
			 * 
			 * @return A reference to this.
			 * 
			 * @sa cpm::importance::upper
			 */
			importance& operator++();
			/*!
			 * @brief Lowers the importance.
			 * 
			 * Appends `lower` to the value.
			 * 
			 * @return A reference to this.
			 * 
			 * @sa cpm::importance::lower
			 */
			importance& operator--();
			
			/*!
			 * @brief The list containing the `upper`s and `lower`s.
			 * 
			 * true = upper
			 * false = lower
			 */
			std::vector<bool> suffixes;
	};
	
	/*!
	 * @brief Saves all information needed for determining the right file parser.
	 */
	class file_parser_correctness {
		public:
			/*!
			 * @brief Whether all errors in the file are recoverable.
			 */
			bool recoverable = true;
			
			/*!
			 * @brief The count of syntax errors in the file.
			 */
			std::uint8_t syntax_error_count = 0;
			
			/*!
			 * @brief The count of used variables.
			 * 
			 * More is better. Also temporary variables belong here.
			 */
			std::uint8_t used_variables = 0;
			
			/*!
			 * @brief How likely it is that this is the right parser.
			 * 
			 * If there are multiple parsers having the same count
			 * of syntax errors, this decides which to use.
			 */
			importance overwrite;
			
			/*!
			 * @brief Convenience function for increasing the syntax error count.
			 * 
			 * It also takes care of overflows.
			 * 
			 * If it returns true, it's hopeless and the function should stop trying
			 * to parse the file as there are enough syntax errors to trigger an
			 * overflow.
			 * 
			 * @return Whether stopping parsing the file is better.
			 */
			bool syntax_error();
			
			/*!
			 * @brief Increase used variables count.
			 * 
			 * Also takes care of overflows.
			 */
			void variable_use();
	};
	
	/*!
	 * @brief Holds utilities for the parser.
	 * 
	 * Currently, only logging functions.
	 * 
	 * The logging functions starting with syntax_
	 * are just file-dependent. They include things
	 * such as syntax errors.
	 * 
	 * The other logging functions are independent
	 * of the contents of the file. An example is
	 * that no free memory to store the new data
	 * is availible.
	 * 
	 * The syntax logging functions will be shown to
	 * the user if he wants to debug his configuration.
	 */
	class file_parser_configuration {
		public:
			/*!
			 * @brief The type of logging functions.
			 */
			typedef std::function<void(const std::string& message)> logcmd;
			
			/*!
			 * @brief The command to call on a general error.
			 * 
			 * Syntax errors not included. Just errors independent
			 * of the file contents.
			 * 
			 * Errors are by definition unrecoverable.
			 * 
			 * Examples are:
			 * - no availible memory
			 * - invalid file stream
			 */
			logcmd error;
			
			/*!
			 * @brief The command to call on syntax errors.
			 * 
			 * `error` means in this case something unrecoverable.
			 * Examples for that are:
			 * - EOF in quoted string: Maybe the wrong quoting
			 *   happened earlier and inversed many strings:
			 *   
			 *   	var a = "quoting error
			 *   	var b = "abcdef"
			 *   	done
			 *   	# missing quote(")
			 *   
			 *   I guess this makes it clear: `var b = ` is part
			 *   of the quoted string after `var a`.
			 */
			logcmd syntax_error;
			
			/*!
			 * @brief The command to call on general warnings.
			 * 
			 * This excludes syntax warnings.
			 */
			logcmd warn;
			
			/*!
			 * @brief The command to call on syntax warnings.
			 * 
			 * Syntax warnings may end in loss of a single variable,
			 * but not until the rest of the file.
			 * 
			 * If you have single line quoting, this may be an
			 * example case:
			 * 
			 * 	var a = "quoting error
			 * 	var b = "something"
			 * 	# For text editors marking quoting: "
			 * 
			 * Here, no line is lost. Even if your syntax doesn't
			 * allow recovering quoting, only that line will be lost.
			 * The following lines can still be parsed independently.
			 */
			logcmd syntax_warn;
			
			/*!
			 * @brief Unlikely syntax.
			 * 
			 * Call this syntax if a syntax is being used which isn't
			 * well known to many users which is more likely to be
			 * meant as another syntax.
			 * 
			 * For example, in bash, imaging `${var##sth}` isn't well
			 * known but `${var#sth}` is:
			 * 
			 * 	${var##sth}
			 * 
			 * Here, this function should be called. A suggestion
			 * should be
			 * 
			 * 	${var#sth}
			 */
			logcmd syntax_unlikely;
			
			/*!
			 * @brief Complicated structure.
			 * 
			 * Call this function if the file has a hard to understand part (for humans).
			 */
			logcmd syntax_complicated;
			
			/*!
			 * @brief Rare syntax usage.
			 * 
			 * Call this function if rare syntax is being used.
			 */
			logcmd syntax_rare;
			
			/*!
			 * @brief Debugging information.
			 * 
			 * Use this for logging debugging information.
			 */
			logcmd debug;
	};
	
	/*!
	 * @brief A file parser.
	 * 
	 * It should be able to parse a specific file type.
	 * 
	 * @tparam result The result type.
	 */
	template<class result>
	class file_parser {
		public:
			/*!
			 * @brief The function for parsing the file.
			 * 
			 * @param stream The file stream to read from. Caller is responsible for opening.
			 * @param r The result to write to.
			 * @param conf The general parser configuration. Mainly about logging.
			 * @return An @ref importance. The result of the parser returning the highest @ref importance will be used.
			 * 
			 * @sa cpm::file_parser_configuration
			 */
			std::function<file_parser_correctness(std::basic_istream<char>& stream, result& r, const file_parser_configuration& conf)> read_function;
			
			/*!
			 * @brief A constructor for setting everything.
			 * 
			 * Currently, just the function has to be set.
			 * 
			 * In the future there may come optional parameters.
			 * 
			 * @param parser The function parsing a file.
			 */
			file_parser(std::function<file_parser_correctness(std::basic_istream<char>& stream, result& r, const file_parser_configuration& conf)> parser) : read_function(parser) {}
			
			/*!
			 * @brief Parse a specific file type.
			 * 
			 * @copydoc read_function
			 */
			file_parser_correctness read(std::basic_istream<char>& stream, result& r, const file_parser_configuration& conf) {
				return read_function(stream, r, conf);
			}
			
			/*!
			 * @brief Parse a specific file type.
			 * 
			 * @copydoc read_function
			 */
			file_parser_correctness operator()(std::basic_istream<char>& stream, result& r, const file_parser_configuration& conf) {
				return read_function(stream, r, conf);
			}
	};
	
	
	/*!
	 * @brief System for handling bitwise-or-able enumerations.
	 * 
	 * Note: The following is written as if it wouldn't be in
	 * the namespace cpm. If you want me to move this into a
	 * library for such a purpose (instead of a packaging
	 * library), feel free to report it (e.g. as issue).
	 * 
	 * The first template parametre should be the enum type.
	 * Make sure to give it an unsigned base type.
	 * The second should specify the highest value the enum has.
	 * It may be good to add a last value, like this:
	 * 
	 * 	enum myflags : unsigned char {
	 * 		VAL1,
	 * 		VAL2,
	 * 		LAST=VAL2
	 * 	}
	 * 	// Then you can use this:
	 * 	flags<myflags, myflags::LAST>
	 * 
	 * If you do this, you may prefer `enum class` over `enum`.
	 * 
	 * For generating a flagset out of your enum, do this:
	 * 
	 * 	flags<enumtype>() << enumtype::item
	 * 
	 * For adding a bit, use bitwise or, as usual. This is made
	 * like this to make the following possible (based on operator
	 * precederence):
	 * 
	 * 	flags<enumtype>() << enumtype::item1 | enumtype::item2
	 * 
	 * If you prefer to construct it out of a enumtype value, that
	 * is possible, too:
	 * 
	 * 	flags<enumtype>(enumtype::item)
	 * 
	 * Againg, based upon operator precederence, you can do this:
	 * 
	 * 	(flags<enumtype>) enumtype::item1 | enumtype::item2
	 * 
	 * Since it is based upon operator precederence, _don't do this_:
	 * 
	 * 	// WRONG!
	 * 	(flags<enumtype>) (enumtype::item1 | enumtype::item2)
	 * 
	 * Instead, use one of the above ways.
	 * 
	 * Since it is impossible to overload the bitwise-or operator of
	 * a enum, don't do this:
	 * 
	 * 	void myfunc(flags<myfunc_flags> f) {
	 * 		doSth(f);
	 * 	}
	 * 	int main() {
	 * 		// WRONG!
	 * 		myfunc(myfunc_flags::item1 | myfunc_flags::item2)
	 * 	}
	 * 
	 * It will throw an exception if it can't convert the given number
	 * because it is too high. However, in such a case, it doesn't
	 * need to be too high. Don't rely on it!
	 * 
	 * If you want to make this handy to use, it may be useful to
	 * define a typedef:
	 * 
	 * 	typedef flags<myfunc_flags> a_new_name
	 * 
	 * In case you need a naming scheme, here is one which you can use:
	 * The enum is always singular (without s, e.g. myfunc_flag),
	 * the typedef is always plural (with s, e.g. myfunc_flags).
	 * 
	 * @tparam _T The enum. Has to be convertable to std::size_t.
	 * @tparam max The maximum value of the enum _T.
	 */
	template<class _T, std::size_t max>
	class flags {
		private:
			/*!
			 * @brief Saves the actual data.
			 */
			std::bitset<max + 1> content;
		public:
			/*!
			 * @brief Initializes flags to none.
			 * 
			 * No flag will be set.
			 * 
			 * Required for Syntax:
			 * 
			 * 	flags<enumname, maxvalue>() << enumname::val1 | enumname::val2
			 * 	flags<enumname, maxvalue>() << enumname::val1 << enumname::val2
			 */
			flags() {
				content.reset();
			}
			
			/*!
			 * @brief Initializes flags to contain just one value.
			 * 
			 * Just the value given will be set.
			 * 
			 * @param o The value to be set.
			 */
			flags(_T o) {
				content.reset();
				content.set((std::size_t)o, true);
			}
			
			/*!
			 * @brief In-place add a flag.
			 * 
			 * @param o The flag to get set.
			 * @return *this
			 */
			flags<_T, max>& operator<<=(_T o) {
				content.set((std::size_t)o, true);
				return *this;
			}
			
			/*!
			 * @brief In-place add a flag.
			 * 
			 * @param o The flag to be set.
			 * @return *this
			 */
			flags<_T, max>& operator|=(_T o) {
				content.set((std::size_t)o, true);
				return *this;
			}
			
			/*!
			 * @brief In-place merge flags.
			 * 
			 * @param o The flags to be set.
			 * @return *this
			 */
			flags<_T, max>& operator|=(const flags<_T, max>& o) {
				content |= o.content;
				return *this;
			}
			
			/*!
			 * @brief Does the thing expected from the usual ways.
			 * 
			 * Resets all bits except the one given.
			 * 
			 * @param o The flag to check.
			 * @return *this
			 */
			flags<_T, max>& operator&=(_T o) {
				bool a = content.test((std::size_t)o);
				content.reset();
				if(a) {
					content.set((std::size_t)o, true);
				}
				return *this;
			}
			
			/*!
			 * @brief Does the thing expected from the usual ways.
			 * 
			 * Resets all bits except the ones in the given flags.
			 * 
			 * @param o The flags to check.
			 * @return *this
			 */
			flags<_T, max>& operator&=(const flags<_T, max>& o) {
				content &= o.content;
				return *this;
			}
			
			/*!
			 * @brief Adds a flag.
			 * 
			 * Required for Syntax:
			 * 
			 * 	flags<enumname, maxvalue>() << enumname::val1 | enumname::val2
			 * 	flags<enumname, maxvalue>() << enumname::val1 << enumname::val2
			 * 
			 * @param o The flag to set.
			 * @return A set of flags additionally containing the given flag.
			 */
			flags<_T, max> operator<<(_T o) const {
				flags<_T, max> r = *this;
				r <<= o;
				return r;
			}
			
			/*!
			 * @brief Adds a flag.
			 * 
			 * Required for Syntax:
			 * 
			 * 	flags<enumname, maxvalue>() << enumname::val1 | enumname::val2
			 * 	flags<enumname, maxvalue>() << enumname::val1 << enumname::val2
			 * 	(flags<enumname, maxvalue>) enumname::val1 | enumname::val2
			 * 
			 * @param o The flag to set.
			 * @return A set of flags additionally containing the given flag.
			 */
			flags<_T, max> operator|(_T o) const {
				flags<_T, max> r = *this;
				r |= o;
				return r;
			}
			
			/*!
			 * @brief Adds flags.
			 * 
			 * This is the expected behaviour if you know the usual way of doing this.
			 * 
			 * Required if you want to combine multiple collections of flags.
			 * 
			 * @param o Flags containing all additional flags.
			 * @return A set of flags containing the flags of this and the given set.
			 */
			flags<_T, max> operator|(const flags<_T, max>& o) const {
				flags<_T, max> r = *this;
				r |= o;
				return r;
			}
			
			/*!
			 * @brief Checks whether a flag is set.
			 * 
			 * Returns a set of flags containing just this flag if it is set, otherwise
			 * containing none.
			 * 
			 * This is the expected behaviour if you know the usual way of doing this.
			 * 
			 * @param o The flag to check.
			 * @return A set of flags just containing the given flag if set.
			 */
			flags<_T, max> operator&(_T o) const {
				flags<_T, max> r = *this;
				r &= o;
				return r;
			}
			
			/*!
			 * @brief Checks whether a flag is set.
			 * 
			 * Returns a set of flags containing just this flag if it is set, otherwise
			 * containing none.
			 * 
			 * This is the expected behaviour if you know the usual way of doing this.
			 * 
			 * @param o The flag to check.
			 * @return A set of flags just containing the given flag if set.
			 */
			flags<_T, max> operator[](_T o) const {
				flags<_T, max> r = *this;
				r &= o;
				return r;
			}
			
			/*!
			 * @brief Checks whether flags are set.
			 * 
			 * Returns a set of flags just containing the flags which are set in both.
			 * 
			 * This is the expected behaviour if you know the usual way of doing this.
			 * 
			 * @param o Flags to check.
			 * @return A set of flags containing the given flags which are set.
			 */
			flags<_T, max> operator&(const flags<_T, max>& o) const {
				flags<_T, max> r = *this;
				r &= o;
				return r;
			}
			
			/*!
			 * @brief True if containing any flag.
			 * 
			 * False otherwise.
			 */
			operator bool() const {
				return content.any();
			}
			
			/*!
			 * @brief Opposite of operator bool.
			 * 
			 * True if no flag is set, false otherwise.
			 */
			bool operator!() const {
				return content.none();
			}
			
			/*!
			 * @brief Checks whether a flag is set.
			 * 
			 * @param f The flag to check.
			 * @return Whether the given flag is set.
			 */
			bool isSet(_T f) const {
				return content[(std::size_t)f];
			}
			
			/*!
			 * @brief Remove a flag.
			 * 
			 * @param o The flag to remove.
			 * @return A reference to this.
			 */
			flags<_T, max> rm(_T o) {
				content.set((std::size_t) o, false);
				return *this;
			}
			
			/*!
			 * @brief Checks whether the same flags are set.
			 * 
			 * This is the expected way if you know the usual way of doing this.
			 * 
			 * @return Whether both flag sets contain the same flags.
			 */
			bool operator==(const flags<_T, max>& o) const {
				return content == o.content;
			}
			
			/*!
			 * @brief Checks whether NOT the same flags are set.
			 * 
			 * This is the expected way if you know the usual way.
			 * 
			 * @return Whether both flag sets DON'T contain the same flags.
			 */
			bool operator!=(const flags<_T, max>& o) const {
				return !*this == o;
			}
	};
	
	
	/*!
	 * @brief An abstract object.
	 * 
	 * Every abstract class should inherit from this because this has the functions
	 * you usually need.
	 */
	class abstract_object {
		public:
			/*!
			 * @brief Copy an object.
			 * 
			 * This is required because you can't otherwise copy an
			 * object from an unknown type.
			 */
			virtual abstract_object* clone() const = 0;
	};
	
	
	/*!
	 * @brief A standardized object.
	 * 
	 * A standardized object is any object conforming to a standard.
	 * A standard was a class before the introduction of this new
	 * concept.
	 * 
	 * A standard is identified by a pointer, meaning you can use
	 * any pointer you like. It doesn't even need to be valid! It
	 * is just for comparison. However, it is recommended having
	 * a valid pointer to an object controlled by you.
	 */
	class standardized : public abstract_object {
		public:
			virtual standardized* clone() const = 0;
			
			/*!
			 * @brief An unique identifier for a standard.
			 */
			virtual const void* stdIdentifier() const = 0;
	};
	
	/*!
	 * @brief A name of anything.
	 * 
	 * It is @ref standardized.
	 * 
	 * @sa cpm::standard
	 * @sa cpm::name_standard
	 * @sa cpm::standardized
	 */
	class name : public standardized {
		public:
			virtual name* clone() const = 0;
			
			/*!
			 * @brief A destructor doing nothing.
			 * 
			 * @sa cpm::standardized::~standardized()
			 */
			virtual ~name();
			
			/*!
			 * @brief Compares whether two names are the same.
			 * 
			 * @param o The name to compare to.
			 * 
			 * @return Whether both are the same.
			 */
			virtual bool operator==(const name& o) const = 0;
			
			/*!
			 * @brief Compares whether two names aren't the same.
			 * 
			 * @param o The name to compare to.
			 * 
			 * @return Whether both aren't the same.
			 */
			bool operator!=(const name& o) const;
			
			/*!
			 * @brief Converts a name to a string.
			 * 
			 * @return A std::string containing the name.
			 * 
			 * @sa cpm::name::operator char*() const
			 * @sa cpm::name::toString() const
			 */
			operator std::string() const;
			
			/*!
			 * @brief Converts a name to a c style string.
			 * 
			 * The result has to be free-ed.
			 * 
			 * @return A char-Array containing the name. Has to be free-ed.
			 * 
			 * @sa cpm::name::operator std::string() const
			 * @sa cpm::name::toCharArray() const
			 */
			operator char*() const;
			
			/*!
			 * @brief Converts a name to a string.
			 * 
			 * @return A std::string containing the name.
			 * 
			 * @sa cpm::name::toCharArray() const
			 * @sa cpm::name::operator std::string() const
			 */
			virtual std::string toString() const = 0;
			
			/*!
			 * @brief Converts a name to a c style string.
			 * 
			 * The result has to be free-ed.
			 * 
			 * @return A char-Array containing the name. Has to be free-ed.
			 * 
			 * @sa cpm::name::toString() const
			 * @sa cpm::name::operator char*() const
			 */
			virtual char* toCharArray() const = 0;
	};
	
	/*!
	 * @brief A version.
	 * 
	 * @sa cpm::standardized
	 */
	class version : public standardized {
		public:
			virtual version* clone() const = 0;
			
			/*!
			 * @brief A destructor doing nothing.
			 * 
			 * @sa cpm::standardized::~standardized()
			 */
			virtual ~version();
			
			/*!
			 * @brief Compares two versions.
			 * 
			 * This function is for determining whether a package should be upgraded.
			 * 
			 * In compairson to determining the most recent version, it may be that an older version will compile to the same code and just has new settings and similar. In this case, this function should return false.
			 * 
			 * Returns false if the standards don't match.
			 * 
			 * @param o The version this version should be compaired to.
			 * @return false if this version should be upgraded to o or the other way round, else true.
			 * 
			 * @sa cpm::version::operator!=(const version& o) const
			 * @sa cpm::version::operator<(const version& o) const
			 * @sa cpm::version::operator>(const version& o) const
			 * @sa cpm::version::operator<=(const version& o) const
			 * @sa cpm::version::operator>=(const version& o) const
			 */
			virtual bool operator==(const version& o) const = 0;
			
			/*!
			 * @brief Compares two versions.
			 * 
			 * This function is for determining whether a package should be upgraded.
			 * 
			 * In compairson to determining the most recent version, it may be that an older version will compile to the same code and just has new settings and similar. In this case, this function should return false.
			 * 
			 * Returns true if the standards don't match.
			 * 
			 * Internally, it is treated as !(*this == o).
			 * 
			 * @param o The version this version should be compaired to.
			 * @return true if this version should be upgraded to o or the other way round, else false.
			 * 
			 * @sa cpm::version::operator==(const version& o) const
			 * @sa cpm::version::operator<(const version& o) const
			 * @sa cpm::version::operator>(const version& o) const
			 * @sa cpm::version::operator<=(const version& o) const
			 * @sa cpm::version::operator>=(const version& o) const
			 */
			bool operator!=(const version& o) const;
			
			/*!
			 * @brief Compares two versions.
			 * 
			 * This function is for determining the most recent version.
			 * 
			 * Returns false if the standards don't match.
			 * 
			 * @param o The version this version should be compaired to.
			 * @return true if this version is older than the other version.
			 * 
			 * @sa cpm::version::operator==(const version& o) const
			 * @sa cpm::version::operator!=(const version& o) const
			 * @sa cpm::version::operator>(const version& o) const
			 * @sa cpm::version::operator<=(const version& o) const
			 * @sa cpm::version::operator>=(const version& o) const
			 */
			virtual bool operator<(const version& o) const = 0;
			
			/*!
			 * @brief Compares two versions.
			 * 
			 * Internally returns (*this < o) || (*this == o)
			 * 
			 * Returns false if the standards don't match.
			 * 
			 * @param o the version to compare this version to.
			 * @return false if this version should be upgraded to o, else true.
			 * 
			 * @sa cpm::version::operator==(const version& o) const
			 * @sa cpm::version::operator!=(const version& o) const
			 * @sa cpm::version::operator<(const version& o) const
			 * @sa cpm::version::operator>(const version& o) const
			 * @sa cpm::version::operator>=(const version& o) const
			 */
			bool operator<=(const version& o) const;
			
			/*!
			 * @brief Compares two versions.
			 * 
			 * This function is for determining the most recent version.
			 * 
			 * Returns false if the standards don't match.
			 * 
			 * @param o The version this version should be compaired to.
			 * @return true if this version is newer than the other version.
			 * 
			 * @sa cpm::version::operator==(const version& o) const
			 * @sa cpm::version::operator!=(const version& o) const
			 * @sa cpm::version::operator<(const version& o) const
			 * @sa cpm::version::operator<=(const version& o) const
			 * @sa cpm::version::operator>=(const version& o) const
			 */
			virtual bool operator>(const version& o) const = 0;
			
			/*!
			 * @brief Compares two versions.
			 * 
			 * Internally returns (*this > o) || (*this == o)
			 * 
			 * Returns false if the standards don't match.
			 * 
			 * Although this method seems pointless to me, I'll keep it, as I don't know whether anyone will find a use case for it.
			 * 
			 * @param o the version to compare this version to.
			 * @return (*this > o) || (*this == o); as above.
			 * 
			 * @sa cpm::version::operator==(const version& o) const
			 * @sa cpm::version::operator!=(const version& o) const
			 * @sa cpm::version::operator<(const version& o) const
			 * @sa cpm::version::operator>(const version& o) const
			 * @sa cpm::version::operator<=(const version& o) const
			 */
			bool operator>=(const version& o) const;
	};
	
	/*!
	 * @brief A node in the package tree.
	 * 
	 * The packages are organized in a tree, like this:
	 * 
	 * 	           <root node>
	 * 	          /           \
	 * 	     repo1             repo2
	 * 	    /     \           /     \
	 * 	pkg1       pkg2   cat1       cat2
	 * 	                 /    \     /    \
	 * 	             pkg1    pkg2 pkg3    pkg4
	 * 
	 * - pkg = package
	 * - cat = category (not all standards support this)
	 * - repo = repository
	 * 
	 * In the example above, the repositories and categories are pkgnodes.
	 */
	class pkgnode : public standardized {
		public:
			virtual pkgnode* clone() const = 0;
			virtual ~pkgnode();
			
			/*!
			 * @brief Checks whether two pkgnodes are the same.
			 */
			virtual bool operator==(const pkgnode& other) const = 0;
			
			/*!
			 * @brief Checks whether two pkgnodes aren't the same.
			 */
			bool operator!=(const pkgnode& other) const;
			
			/*!
			 * @brief child package nodes (const version).
			 * 
			 * Children of this node in the tree described in @ref pkgnode.
			 * 
			 * @return A std::vector of all child nodes.
			 */
			virtual std::vector<const pkgnode*> getChildren() const = 0;
			
			/*!
			 * @brief child package nodes (non-const version).
			 * 
			 * Children of this node in the tree described in @ref pkgnode.
			 * 
			 * @return A std::vector of all child nodes.
			 */
			virtual std::vector<pkgnode*> getChildren() = 0;
			
			/*!
			 * @brief packages in this node (const version).
			 * 
			 * Just returns the packages contained directly in this @ref pkgnode, doesn't
			 * search child nodes for more packages.
			 * 
			 * @return A std::vector of the packages contained.
			 */
			virtual std::vector<const package*> getPackages() const = 0;
			
			/*!
			 * @brief packages in this node (non-const version).
			 * 
			 * Just returns the packages contained directly in this @ref pkgnode, doesn't
			 * search child nodes for more packages.
			 * 
			 * @return A std::vector of the packages contained.
			 */
			virtual std::vector<package*> getPackages() = 0;
			
			/*!
			 * @brief The parent.
			 * 
			 * Should return null if it would be the root node.
			 */
			virtual const pkgnode* getParent() const = 0;
			
			/*!
			 * @brief Should return the preferred name for the user.
			 * 
			 * Sometimes, one may wish to call a repository different than many repositories
			 * expect it to be called (by cross-reference). This should return the name the
			 * user wants to read.
			 * 
			 * If this feature isn't supported by the configuration file format
			 * (most don't support it), both names can be equal.
			 */
			virtual const name* getNameUserPreferred() const = 0;
	};
	
	/*!
	 * @brief Repository.
	 * 
	 * A repository is a collection of packages provided by a provider.
	 * 
	 * A provider is a grouop of people or just a single person owning
	 * a repository. Some providers own multiple repositories.
	 * 
	 * Sometimes it is also used for grouping packages. This is
	 * especially true for package managers not supporting categories
	 * or similar grouping mechanisms, like apt or rpm. Actually,
	 * there are less package managers supporting such a feature than
	 * not supporting such a feature, I think.
	 * 
	 * @sa cpm::pkgnode
	 */
	class repository : public pkgnode {
		public:
			virtual repository* clone() const = 0;
			virtual ~repository();
	};
	
	/*!
	 * @brief A package.
	 * 
	 * A package usually is software. It could be a program or library,
	 * for example. However, it can be anything that can be installed,
	 * meaning it could also be a icon theme.
	 * 
	 * It is a very common term in package management.
	 */
	class package : public standardized {
		public:
			virtual package* clone() const = 0;
			virtual ~package();
	};
	
	/*!
	 * @brief A logging message.
	 */
	class logging_message {
		public:
			/*!
			 * @brief The message to be logged.
			 */
			std::string message;
			
			/*!
			 * @brief The logging_level of this message.
			 */
			logging_level level;
			
			/*!
			 * @brief The time this was logged.
			 */
			std::time_t time;
			
			/*!
			 * @brief The backtrace.
			 * 
			 * Just a list of function identifiers ordered by
			 * the call stack.
			 */
			const std::vector<std::string>* backtrace;
	};
	
	/*!
	 * @brief A configuration for libcpm.
	 * 
	 * It saves all configuration data.
	 */
	class configuration {
		public:
			configuration();
			
			/*!
			 * @brief All repositories.
			 */
			std::vector<repository*> repositories;
			
			/*!
			 * @brief Merges two configurations.
			 * 
			 * The other configuration has precedance.
			 * 
			 * @param c The libcpm context.
			 * @param o The other configuration.
			 */
			void update(context& c, configuration& o);
	};
	
	/*!
	 * @brief A context for libcpm.
	 * 
	 * It saves a @ref configuration and some run-time data.
	 */
	class context {
		private:
			std::vector<std::string> backtrace;
			friend class log;
		public:
			/*!
			 * @brief The configuration used in this context.
			 */
			configuration conf;
			
			/*!
			 * @brief A list of parsers for configurations.
			 * 
			 * If a configuration file should be read, this
			 * is a list of functions trying to read that
			 * file.
			 * 
			 * The file is the first parameter, the second
			 * is the @ref configuration to update using
			 * the file.
			 */
			std::vector<file_parser<configuration>> configuration_file_parsers;
			
			/*!
			 * @brief Generates a new context.
			 */
			context();
			
			/*!
			 * @brief The logging function.
			 * 
			 * It should handle logging operations in libcpm.
			 * 
			 * It gets all the metadata needed as a @ref logging_message.
			 * It is actually a temporary value. For performance reasons
			 * it is allowed to be changed.
			 */
			std::function<void(logging_message&&)> logging_function;
			
			/*!
			 * @brief Parse the given configuration file.
			 * 
			 * It searches through @ref configuration_file_parsers, lets
			 * each one try to parse the file and keep just the best.
			 * 
			 * Then, @ref conf will be updated using that data.
			 * 
			 * @param path The path of the file to parse
			 */
			void parse(const std::string& path);
	};
}

#endif
