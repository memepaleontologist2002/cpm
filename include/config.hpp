/*!
 * @file A wrapper file around config.h.
 * 
 * It has an include guard.
 */

#ifndef CONFIG_HPP
#define CONFIG_HPP

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#endif
