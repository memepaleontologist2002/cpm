# CPM
[![pipeline status](https://shields.io/gitlab/pipeline/TheDreche/cpm/main?logo=gitlab)](https://gitlab.com/TheDreche/cpm/-/pipelines) [![lines of code](https://shields.io/tokei/lines/gitlab/TheDreche/cpm)](https://gitlab.com/TheDreche/cpm/)

Cpm will be a package manager with many features. However, to make it odd, the focus is not on usability, but configurability.

## Configure options
- `--enable-debug` sets default logging level to debug (once implemented) and does some things intended for use by developers.
- `--enable-doc` sets the default value for all `--enable-doc-XXX` options, each enabling generating documentation in one more format using doxygen. Currently, two of these are implemented:
  * `--enable-doc-html`: HTML documentation
  * `--enable-doc-man`: Man pages
  Later, maybe more options will be implemented.
- `--enable-std-*` adds support for a standard. Supported standards:
  * `--enable-std-eapi`

## Documentation
The source code is documented using [doxygen](https://www.doxygen.nl/). Pass the `--enable-doc` option to `./configure` to generate all documentation. (For more details, see Configure options)

## Versioning
This project uses [semantic versioning](https://semver.org).

## Dependencies
* C++17
* gmpxx (gmp with c++ support)

## Configuration file format
If not using a non-default configuration file format, a big extension to `.ini` is used:
* `[]` (empty section) is allowed, by default everything is in there. (everything at the beginning of a file)
  - If you don't get confused: every section, even the empty section, has an empty section in it, containing all variables of the section
* Comments:
  - `#`
  - `;`
  - `//`
  - `/* ... */`
* subsections
* subsection separators:
  - `::`
  - `.`
  - `/`
* Sub-sections
* section identifiers:
  - `section`: The top-level section `section`
  - `::subsection`: The subsection of this section called `subsection` (:: can be any subsection separator, `/` has special meaning, see next point)
  - `/subsection`: The subsection of the parent section (same as `../subsection`, `..::subsection`, `...subsection` and similar, see below)
  - `..`: The section above
  - These can be combined: `..::..::secname`
  - Not recommended, but possible: `...secname` means the section `secname` of the above section (first 2 dots are above section, third section separator)
  These section identifiers are used everywhere where usual:
  
  	[sectionidentifier]
  
* Variable identifiers:
  - `[section]::var` (:: can be any subsection separator)
  - `[]::var` (same as abve) for variables in the current section
* New keywords:
  - `import "filename"`: Also read file `filename`
  - `import <filename>`: Also read file `filename`, but search in default path (explained later)
  - `include "filename"`: Reads another file here (the beginning belongs to the current section), search in the directory of the file
  - `include <filename>`: Reads another file here (the beginning belongs to the current section), search in default path
  - `use [section]`: Insert values and subsections of section `section` into the current section
  - `use [variable]::identifier`: Use variable identified by the identifier
* Sub-section-separators: `::` and `.`
* Overridable replacements:
  A set of brackets, which are passed to an interpreter, such as a shell.
  You can override a set of brackets like this:
  
  	[]::() = sh
  
  After that, everything in $(...) will be passed to /bin/sh. 
  They are like special variables in the global empty section, with special meaning.
  
  An example after the above line:
  
  	$(echo 'var = thing')
  
  Will run `echo var = thing` in `/bin/sh`, read stdout and interpret that.
  
  	(var=thing)
  
  will run `var=thing` in a shell and save the environment after it for later use by `$(...)`.
  It has to be in a own line!
  
  Brackets whch can be overwritten:
  - `()`
  - `[]`
  - `{}`
  - `""`
  - `''`
  - `<>`
  What they can mean: (if they are used for `()`)
  *quite sure*:
  - `char`: Read as if it wouldn't be a bracket at all)
  - `ignore`: just read content as usual
  - `quote`: Use for quoting
  - `var`: Read variable identifier
  - `env`: replace `$()` by environment variable
  - `env+`: replace `$()` by environment variable, but read like bash (so, `$(var:=val)` will be parsed like bash would do with `{}`)
  - `math`: interpret mathematical equasion (won't support every possible equasion)
  *if possible*:
  - `sh`: Pase using a shell (`/bin/sh` should point to one)
  - `scheme`: Parse with a scheme interpreter (currently same as `guile`)
  - `guile`: Parse using guile
  - `python`: Parse using the latest installd python, although maybe won't be implemented (if goal will be fulfilled, a version can be appended, like the command)
  Default settings will be:
  - `[]::() = scheme`
  - `[]::[] = math`
  - `[]::{} = env+`
  - `[]::"" = quote`
  - `[]::'' = quote`
  - `[]::<> = var`
  You can do funny things with them, such as
  - `name = ${user:=default}`
  - using sh: `$(if ilikeuser; then echo 'colour = blue'; else echo 'colour = black; fi')`

## Other information
* Until final 0.0.0, the changelog and news won't be written.

## Next steps
* [ ] Implement the basic classes:
  - [ ] repository
  - [ ] structurenode
  - [ ] package
  - [ ] slot
  - [ ] pkgversion
  - [ ] pkgbuild
  - [ ] way
  - [ ] step
* [ ] Add configuration file parsing
  - [x] Think of a good configuration file format
* [ ] Logging
* [ ] Add gpgme support
* [ ] Maybe tests like unit tests?
* [ ] Maybe drawing a logo for the project?
